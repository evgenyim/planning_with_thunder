# CMake generated Testfile for 
# Source directory: /home/sirius/planning_with_thunder/new_workspace/src
# Build directory: /home/sirius/planning_with_thunder/new_workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("msgs/rtp_msgs")
subdirs("msgs/pnc_task_msgs")
subdirs("msgs/thunder_msgs")
subdirs("test_package")
