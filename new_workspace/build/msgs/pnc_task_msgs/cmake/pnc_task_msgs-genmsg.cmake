# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "pnc_task_msgs: 2 messages, 0 services")

set(MSG_I_FLAGS "-Ipnc_task_msgs:/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg;-Inav_msgs:/opt/ros/noetic/share/nav_msgs/cmake/../msg;-Irtp_msgs:/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg;-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/noetic/share/actionlib_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(pnc_task_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_custom_target(_pnc_task_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "pnc_task_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" "rtp_msgs/PathPointWithMetadata:std_msgs/Header:geometry_msgs/PoseWithCovariance:rtp_msgs/PathPointMetadata:geometry_msgs/Vector3:nav_msgs/MapMetaData:geometry_msgs/TwistWithCovariance:geometry_msgs/Twist:geometry_msgs/Pose:rtp_msgs/PathStamped:nav_msgs/Odometry:geometry_msgs/Point:geometry_msgs/Quaternion:nav_msgs/OccupancyGrid"
)

get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_custom_target(_pnc_task_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "pnc_task_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" "rtp_msgs/PathPointWithMetadata:std_msgs/Header:rtp_msgs/PathPointMetadata:nav_msgs/MapMetaData:geometry_msgs/Pose:geometry_msgs/Point:geometry_msgs/Quaternion:nav_msgs/OccupancyGrid"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseWithCovariance.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/TwistWithCovariance.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Twist.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/Odometry.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/pnc_task_msgs
)
_generate_msg_cpp(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/pnc_task_msgs
)

### Generating Services

### Generating Module File
_generate_module_cpp(pnc_task_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/pnc_task_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(pnc_task_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(pnc_task_msgs_generate_messages pnc_task_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_cpp _pnc_task_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_cpp _pnc_task_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(pnc_task_msgs_gencpp)
add_dependencies(pnc_task_msgs_gencpp pnc_task_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS pnc_task_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseWithCovariance.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/TwistWithCovariance.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Twist.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/Odometry.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/pnc_task_msgs
)
_generate_msg_eus(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/pnc_task_msgs
)

### Generating Services

### Generating Module File
_generate_module_eus(pnc_task_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/pnc_task_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(pnc_task_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(pnc_task_msgs_generate_messages pnc_task_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_eus _pnc_task_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_eus _pnc_task_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(pnc_task_msgs_geneus)
add_dependencies(pnc_task_msgs_geneus pnc_task_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS pnc_task_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseWithCovariance.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/TwistWithCovariance.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Twist.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/Odometry.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/pnc_task_msgs
)
_generate_msg_lisp(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/pnc_task_msgs
)

### Generating Services

### Generating Module File
_generate_module_lisp(pnc_task_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/pnc_task_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(pnc_task_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(pnc_task_msgs_generate_messages pnc_task_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_lisp _pnc_task_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_lisp _pnc_task_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(pnc_task_msgs_genlisp)
add_dependencies(pnc_task_msgs_genlisp pnc_task_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS pnc_task_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseWithCovariance.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/TwistWithCovariance.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Twist.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/Odometry.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/pnc_task_msgs
)
_generate_msg_nodejs(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/pnc_task_msgs
)

### Generating Services

### Generating Module File
_generate_module_nodejs(pnc_task_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/pnc_task_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(pnc_task_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(pnc_task_msgs_generate_messages pnc_task_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_nodejs _pnc_task_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_nodejs _pnc_task_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(pnc_task_msgs_gennodejs)
add_dependencies(pnc_task_msgs_gennodejs pnc_task_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS pnc_task_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/PoseWithCovariance.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Vector3.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/TwistWithCovariance.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Twist.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/Odometry.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs
)
_generate_msg_py(pnc_task_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg"
  "${MSG_I_FLAGS}"
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/MapMetaData.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/nav_msgs/cmake/../msg/OccupancyGrid.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs
)

### Generating Services

### Generating Module File
_generate_module_py(pnc_task_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(pnc_task_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(pnc_task_msgs_generate_messages pnc_task_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/ControlTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_py _pnc_task_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/pnc_task_msgs/msg/PlanningTask.msg" NAME_WE)
add_dependencies(pnc_task_msgs_generate_messages_py _pnc_task_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(pnc_task_msgs_genpy)
add_dependencies(pnc_task_msgs_genpy pnc_task_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS pnc_task_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/pnc_task_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/pnc_task_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(pnc_task_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET nav_msgs_generate_messages_cpp)
  add_dependencies(pnc_task_msgs_generate_messages_cpp nav_msgs_generate_messages_cpp)
endif()
if(TARGET rtp_msgs_generate_messages_cpp)
  add_dependencies(pnc_task_msgs_generate_messages_cpp rtp_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/pnc_task_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/pnc_task_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(pnc_task_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET nav_msgs_generate_messages_eus)
  add_dependencies(pnc_task_msgs_generate_messages_eus nav_msgs_generate_messages_eus)
endif()
if(TARGET rtp_msgs_generate_messages_eus)
  add_dependencies(pnc_task_msgs_generate_messages_eus rtp_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/pnc_task_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/pnc_task_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(pnc_task_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET nav_msgs_generate_messages_lisp)
  add_dependencies(pnc_task_msgs_generate_messages_lisp nav_msgs_generate_messages_lisp)
endif()
if(TARGET rtp_msgs_generate_messages_lisp)
  add_dependencies(pnc_task_msgs_generate_messages_lisp rtp_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/pnc_task_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/pnc_task_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(pnc_task_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET nav_msgs_generate_messages_nodejs)
  add_dependencies(pnc_task_msgs_generate_messages_nodejs nav_msgs_generate_messages_nodejs)
endif()
if(TARGET rtp_msgs_generate_messages_nodejs)
  add_dependencies(pnc_task_msgs_generate_messages_nodejs rtp_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/pnc_task_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(pnc_task_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET nav_msgs_generate_messages_py)
  add_dependencies(pnc_task_msgs_generate_messages_py nav_msgs_generate_messages_py)
endif()
if(TARGET rtp_msgs_generate_messages_py)
  add_dependencies(pnc_task_msgs_generate_messages_py rtp_msgs_generate_messages_py)
endif()
