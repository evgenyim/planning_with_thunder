set(_CATKIN_CURRENT_PACKAGE "rtp_msgs")
set(rtp_msgs_VERSION "0.1.0")
set(rtp_msgs_MAINTAINER "Ivan Radionov <i.a.radinov@gmail.com>")
set(rtp_msgs_PACKAGE_FORMAT "2")
set(rtp_msgs_BUILD_DEPENDS "message_generation" "geometry_msgs")
set(rtp_msgs_BUILD_EXPORT_DEPENDS )
set(rtp_msgs_BUILDTOOL_DEPENDS "catkin")
set(rtp_msgs_BUILDTOOL_EXPORT_DEPENDS )
set(rtp_msgs_EXEC_DEPENDS "message_runtime")
set(rtp_msgs_RUN_DEPENDS "message_runtime")
set(rtp_msgs_TEST_DEPENDS )
set(rtp_msgs_DOC_DEPENDS )
set(rtp_msgs_URL_WEBSITE "")
set(rtp_msgs_URL_BUGTRACKER "")
set(rtp_msgs_URL_REPOSITORY "")
set(rtp_msgs_DEPRECATED "")