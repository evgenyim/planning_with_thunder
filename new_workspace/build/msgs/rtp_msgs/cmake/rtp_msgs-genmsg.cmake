# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "rtp_msgs: 5 messages, 0 services")

set(MSG_I_FLAGS "-Irtp_msgs:/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg;-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(rtp_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_custom_target(_rtp_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtp_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" "geometry_msgs/Point:rtp_msgs/PathPointMetadata:std_msgs/Header:rtp_msgs/PathPointWithMetadata:geometry_msgs/Quaternion:rtp_msgs/RouteMetadata:geometry_msgs/Pose"
)

get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_custom_target(_rtp_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtp_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" ""
)

get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_custom_target(_rtp_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtp_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" ""
)

get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_custom_target(_rtp_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtp_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" "geometry_msgs/Pose:geometry_msgs/Quaternion:geometry_msgs/Point:rtp_msgs/PathPointMetadata"
)

get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_custom_target(_rtp_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "rtp_msgs" "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" "geometry_msgs/Point:rtp_msgs/PathPointMetadata:std_msgs/Header:rtp_msgs/PathPointWithMetadata:geometry_msgs/Quaternion:geometry_msgs/Pose"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_cpp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_cpp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_cpp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_cpp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
)

### Generating Services

### Generating Module File
_generate_module_cpp(rtp_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(rtp_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(rtp_msgs_generate_messages rtp_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_cpp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_cpp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_cpp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_cpp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_cpp _rtp_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtp_msgs_gencpp)
add_dependencies(rtp_msgs_gencpp rtp_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtp_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
)
_generate_msg_eus(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
)
_generate_msg_eus(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
)
_generate_msg_eus(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
)
_generate_msg_eus(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
)

### Generating Services

### Generating Module File
_generate_module_eus(rtp_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(rtp_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(rtp_msgs_generate_messages rtp_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_eus _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_eus _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_eus _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_eus _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_eus _rtp_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtp_msgs_geneus)
add_dependencies(rtp_msgs_geneus rtp_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtp_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_lisp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_lisp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_lisp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
)
_generate_msg_lisp(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
)

### Generating Services

### Generating Module File
_generate_module_lisp(rtp_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(rtp_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(rtp_msgs_generate_messages rtp_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_lisp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_lisp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_lisp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_lisp _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_lisp _rtp_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtp_msgs_genlisp)
add_dependencies(rtp_msgs_genlisp rtp_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtp_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
)
_generate_msg_nodejs(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
)
_generate_msg_nodejs(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
)
_generate_msg_nodejs(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
)
_generate_msg_nodejs(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
)

### Generating Services

### Generating Module File
_generate_module_nodejs(rtp_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(rtp_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(rtp_msgs_generate_messages rtp_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_nodejs _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_nodejs _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_nodejs _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_nodejs _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_nodejs _rtp_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtp_msgs_gennodejs)
add_dependencies(rtp_msgs_gennodejs rtp_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtp_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
)
_generate_msg_py(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
)
_generate_msg_py(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
)
_generate_msg_py(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
)
_generate_msg_py(rtp_msgs
  "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Point.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/noetic/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
)

### Generating Services

### Generating Module File
_generate_module_py(rtp_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(rtp_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(rtp_msgs_generate_messages rtp_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteTask.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_py _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/RouteMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_py _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_py _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathPointWithMetadata.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_py _rtp_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/sirius/planning_with_thunder/new_workspace/src/msgs/rtp_msgs/msg/PathStamped.msg" NAME_WE)
add_dependencies(rtp_msgs_generate_messages_py _rtp_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(rtp_msgs_genpy)
add_dependencies(rtp_msgs_genpy rtp_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS rtp_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/rtp_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(rtp_msgs_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(rtp_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/rtp_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(rtp_msgs_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(rtp_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/rtp_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(rtp_msgs_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(rtp_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/rtp_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(rtp_msgs_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(rtp_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/rtp_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(rtp_msgs_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(rtp_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
