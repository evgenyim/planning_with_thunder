;; Auto-generated. Do not edit!


(when (boundp 'thunder_msgs::ThunderControl)
  (if (not (find-package "THUNDER_MSGS"))
    (make-package "THUNDER_MSGS"))
  (shadow 'ThunderControl (find-package "THUNDER_MSGS")))
(unless (find-package "THUNDER_MSGS::THUNDERCONTROL")
  (make-package "THUNDER_MSGS::THUNDERCONTROL"))

(in-package "ROS")
;;//! \htmlinclude ThunderControl.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*CAN_CONTROL_ID*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CAN_CONTROL_ID* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CAN_CONTROL_ID* 528)
(intern "*CAN_PTZ_ID*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CAN_PTZ_ID* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CAN_PTZ_ID* 353)
(intern "*CONTROL_MODE_NONE*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_MODE_NONE* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_MODE_NONE* 10)
(intern "*CONTROL_MODE_FAN*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_MODE_FAN* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_MODE_FAN* 11)
(intern "*CONTROL_MODE_AUTO*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_MODE_AUTO* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_MODE_AUTO* 12)
(intern "*CONTROL_MODE_PTZ*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_MODE_PTZ* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_MODE_PTZ* 13)
(intern "*FAN_PUMP_NONE*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*FAN_PUMP_NONE* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*FAN_PUMP_NONE* 20)
(intern "*FAN_PUMP_CLOSE*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*FAN_PUMP_CLOSE* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*FAN_PUMP_CLOSE* 22)
(intern "*FAN_PUMP_OPEN*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*FAN_PUMP_OPEN* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*FAN_PUMP_OPEN* 23)
(intern "*CONTROL_SWITCH_NONE*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_SWITCH_NONE* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_SWITCH_NONE* 30)
(intern "*CONTROL_SWITCH_CONTROL*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_SWITCH_CONTROL* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_SWITCH_CONTROL* 32)
(intern "*CONTROL_SWITCH_PTZ*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*CONTROL_SWITCH_PTZ* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*CONTROL_SWITCH_PTZ* 31)
(intern "*PTZ_SWITCH_NONE*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*PTZ_SWITCH_NONE* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*PTZ_SWITCH_NONE* 40)
(intern "*PTZ_SWITCH_MANUAL*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*PTZ_SWITCH_MANUAL* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*PTZ_SWITCH_MANUAL* 42)
(intern "*PTZ_SWITCH_AUTO*" (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(shadow '*PTZ_SWITCH_AUTO* (find-package "THUNDER_MSGS::THUNDERCONTROL"))
(defconstant thunder_msgs::ThunderControl::*PTZ_SWITCH_AUTO* 43)

(defun thunder_msgs::ThunderControl-to-symbol (const)
  (cond
        ((= const 528) 'thunder_msgs::ThunderControl::*CAN_CONTROL_ID*)
        ((= const 353) 'thunder_msgs::ThunderControl::*CAN_PTZ_ID*)
        ((= const 10) 'thunder_msgs::ThunderControl::*CONTROL_MODE_NONE*)
        ((= const 11) 'thunder_msgs::ThunderControl::*CONTROL_MODE_FAN*)
        ((= const 12) 'thunder_msgs::ThunderControl::*CONTROL_MODE_AUTO*)
        ((= const 13) 'thunder_msgs::ThunderControl::*CONTROL_MODE_PTZ*)
        ((= const 20) 'thunder_msgs::ThunderControl::*FAN_PUMP_NONE*)
        ((= const 22) 'thunder_msgs::ThunderControl::*FAN_PUMP_CLOSE*)
        ((= const 23) 'thunder_msgs::ThunderControl::*FAN_PUMP_OPEN*)
        ((= const 30) 'thunder_msgs::ThunderControl::*CONTROL_SWITCH_NONE*)
        ((= const 32) 'thunder_msgs::ThunderControl::*CONTROL_SWITCH_CONTROL*)
        ((= const 31) 'thunder_msgs::ThunderControl::*CONTROL_SWITCH_PTZ*)
        ((= const 40) 'thunder_msgs::ThunderControl::*PTZ_SWITCH_NONE*)
        ((= const 42) 'thunder_msgs::ThunderControl::*PTZ_SWITCH_MANUAL*)
        ((= const 43) 'thunder_msgs::ThunderControl::*PTZ_SWITCH_AUTO*)
        (t nil)))

(defclass thunder_msgs::ThunderControl
  :super ros::object
  :slots (_header _can_id _control_mode _fan_pump _control_switch _ptz_switch _ptz ))

(defmethod thunder_msgs::ThunderControl
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:can_id __can_id) 0)
    ((:control_mode __control_mode) 0)
    ((:fan_pump __fan_pump) 0)
    ((:control_switch __control_switch) 0)
    ((:ptz_switch __ptz_switch) 0)
    ((:ptz __ptz) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _can_id (round __can_id))
   (setq _control_mode (round __control_mode))
   (setq _fan_pump (round __fan_pump))
   (setq _control_switch (round __control_switch))
   (setq _ptz_switch (round __ptz_switch))
   (setq _ptz (round __ptz))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:can_id
   (&optional __can_id)
   (if __can_id (setq _can_id __can_id)) _can_id)
  (:control_mode
   (&optional __control_mode)
   (if __control_mode (setq _control_mode __control_mode)) _control_mode)
  (:fan_pump
   (&optional __fan_pump)
   (if __fan_pump (setq _fan_pump __fan_pump)) _fan_pump)
  (:control_switch
   (&optional __control_switch)
   (if __control_switch (setq _control_switch __control_switch)) _control_switch)
  (:ptz_switch
   (&optional __ptz_switch)
   (if __ptz_switch (setq _ptz_switch __ptz_switch)) _ptz_switch)
  (:ptz
   (&optional __ptz)
   (if __ptz (setq _ptz __ptz)) _ptz)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _can_id
    4
    ;; uint8 _control_mode
    1
    ;; uint8 _fan_pump
    1
    ;; uint8 _control_switch
    1
    ;; uint8 _ptz_switch
    1
    ;; int16 _ptz
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _can_id
       (write-long _can_id s)
     ;; uint8 _control_mode
       (write-byte _control_mode s)
     ;; uint8 _fan_pump
       (write-byte _fan_pump s)
     ;; uint8 _control_switch
       (write-byte _control_switch s)
     ;; uint8 _ptz_switch
       (write-byte _ptz_switch s)
     ;; int16 _ptz
       (write-word _ptz s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _can_id
     (setq _can_id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _control_mode
     (setq _control_mode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _fan_pump
     (setq _fan_pump (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _control_switch
     (setq _control_switch (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _ptz_switch
     (setq _ptz_switch (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; int16 _ptz
     (setq _ptz (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get thunder_msgs::ThunderControl :md5sum-) "5e455a4d42a93ecb6aa73b9ba635c80a")
(setf (get thunder_msgs::ThunderControl :datatype-) "thunder_msgs/ThunderControl")
(setf (get thunder_msgs::ThunderControl :definition-)
      "Header header

uint32 CAN_CONTROL_ID = 528     # 0x210
uint32 CAN_PTZ_ID = 353         # 0x161 

uint8 CONTROL_MODE_NONE = 10
uint8 CONTROL_MODE_FAN = 11
uint8 CONTROL_MODE_AUTO = 12
uint8 CONTROL_MODE_PTZ = 13

uint8 FAN_PUMP_NONE = 20
uint8 FAN_PUMP_CLOSE = 22
uint8 FAN_PUMP_OPEN = 23

uint8 CONTROL_SWITCH_NONE = 30
uint8 CONTROL_SWITCH_CONTROL = 32
uint8 CONTROL_SWITCH_PTZ = 31

uint8 PTZ_SWITCH_NONE = 40
uint8 PTZ_SWITCH_MANUAL = 42
uint8 PTZ_SWITCH_AUTO = 43

# type of can message
uint32 can_id

# control command
uint8 control_mode

# ptz command
uint8 fan_pump
uint8 control_switch
uint8 ptz_switch
int16 ptz

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :thunder_msgs/ThunderControl "5e455a4d42a93ecb6aa73b9ba635c80a")


