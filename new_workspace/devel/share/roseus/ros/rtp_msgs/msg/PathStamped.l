;; Auto-generated. Do not edit!


(when (boundp 'rtp_msgs::PathStamped)
  (if (not (find-package "RTP_MSGS"))
    (make-package "RTP_MSGS"))
  (shadow 'PathStamped (find-package "RTP_MSGS")))
(unless (find-package "RTP_MSGS::PATHSTAMPED")
  (make-package "RTP_MSGS::PATHSTAMPED"))

(in-package "ROS")
;;//! \htmlinclude PathStamped.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*EUCLIDEAN*" (find-package "RTP_MSGS::PATHSTAMPED"))
(shadow '*EUCLIDEAN* (find-package "RTP_MSGS::PATHSTAMPED"))
(defconstant rtp_msgs::PathStamped::*EUCLIDEAN* 0)
(intern "*DUBINS*" (find-package "RTP_MSGS::PATHSTAMPED"))
(shadow '*DUBINS* (find-package "RTP_MSGS::PATHSTAMPED"))
(defconstant rtp_msgs::PathStamped::*DUBINS* 1)
(intern "*REEDS_SHEPP*" (find-package "RTP_MSGS::PATHSTAMPED"))
(shadow '*REEDS_SHEPP* (find-package "RTP_MSGS::PATHSTAMPED"))
(defconstant rtp_msgs::PathStamped::*REEDS_SHEPP* 2)

(defun rtp_msgs::PathStamped-to-symbol (const)
  (cond
        ((= const 0) 'rtp_msgs::PathStamped::*EUCLIDEAN*)
        ((= const 1) 'rtp_msgs::PathStamped::*DUBINS*)
        ((= const 2) 'rtp_msgs::PathStamped::*REEDS_SHEPP*)
        (t nil)))

(defclass rtp_msgs::PathStamped
  :super ros::object
  :slots (_header _path_with_metadata _path_type _route_index_to _route_index_from ))

(defmethod rtp_msgs::PathStamped
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:path_with_metadata __path_with_metadata) ())
    ((:path_type __path_type) 0)
    ((:route_index_to __route_index_to) 0)
    ((:route_index_from __route_index_from) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _path_with_metadata __path_with_metadata)
   (setq _path_type (round __path_type))
   (setq _route_index_to (round __route_index_to))
   (setq _route_index_from (round __route_index_from))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:path_with_metadata
   (&rest __path_with_metadata)
   (if (keywordp (car __path_with_metadata))
       (send* _path_with_metadata __path_with_metadata)
     (progn
       (if __path_with_metadata (setq _path_with_metadata (car __path_with_metadata)))
       _path_with_metadata)))
  (:path_type
   (&optional __path_type)
   (if __path_type (setq _path_type __path_type)) _path_type)
  (:route_index_to
   (&optional __route_index_to)
   (if __route_index_to (setq _route_index_to __route_index_to)) _route_index_to)
  (:route_index_from
   (&optional __route_index_from)
   (if __route_index_from (setq _route_index_from __route_index_from)) _route_index_from)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
    (apply #'+ (send-all _path_with_metadata :serialization-length)) 4
    ;; uint8 _path_type
    1
    ;; uint16 _route_index_to
    2
    ;; uint16 _route_index_from
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
     (write-long (length _path_with_metadata) s)
     (dolist (elem _path_with_metadata)
       (send elem :serialize s)
       )
     ;; uint8 _path_type
       (write-byte _path_type s)
     ;; uint16 _route_index_to
       (write-word _route_index_to s)
     ;; uint16 _route_index_from
       (write-word _route_index_from s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _path_with_metadata (let (r) (dotimes (i n) (push (instance rtp_msgs::PathPointWithMetadata :init) r)) r))
     (dolist (elem- _path_with_metadata)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; uint8 _path_type
     (setq _path_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _route_index_to
     (setq _route_index_to (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _route_index_from
     (setq _route_index_from (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get rtp_msgs::PathStamped :md5sum-) "ebd42ddb68da2c8327f5578deb75232f")
(setf (get rtp_msgs::PathStamped :datatype-) "rtp_msgs/PathStamped")
(setf (get rtp_msgs::PathStamped :definition-)
      "std_msgs/Header header

# Массив, содержащий точки траектории с дополнительными свойствами
rtp_msgs/PathPointWithMetadata[] path_with_metadata

# Тип траектории
uint8 path_type
uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве
uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса
uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа

# Индекс точки маршрута, к которой построена траектория
uint16 route_index_to

# Индекс точки маршрута, от которой построена траектория
uint16 route_index_from
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: rtp_msgs/PathPointWithMetadata
# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
geometry_msgs/Pose pose

# Дополнительные свойства точки маршрута/траектории
rtp_msgs/PathPointMetadata metadata
================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: rtp_msgs/PathPointMetadata
# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
")



(provide :rtp_msgs/PathStamped "ebd42ddb68da2c8327f5578deb75232f")


