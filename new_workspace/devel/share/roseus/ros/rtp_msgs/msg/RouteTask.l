;; Auto-generated. Do not edit!


(when (boundp 'rtp_msgs::RouteTask)
  (if (not (find-package "RTP_MSGS"))
    (make-package "RTP_MSGS"))
  (shadow 'RouteTask (find-package "RTP_MSGS")))
(unless (find-package "RTP_MSGS::ROUTETASK")
  (make-package "RTP_MSGS::ROUTETASK"))

(in-package "ROS")
;;//! \htmlinclude RouteTask.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass rtp_msgs::RouteTask
  :super ros::object
  :slots (_header _path_with_metadata _route_metadata ))

(defmethod rtp_msgs::RouteTask
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:path_with_metadata __path_with_metadata) ())
    ((:route_metadata __route_metadata) (instance rtp_msgs::RouteMetadata :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _path_with_metadata __path_with_metadata)
   (setq _route_metadata __route_metadata)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:path_with_metadata
   (&rest __path_with_metadata)
   (if (keywordp (car __path_with_metadata))
       (send* _path_with_metadata __path_with_metadata)
     (progn
       (if __path_with_metadata (setq _path_with_metadata (car __path_with_metadata)))
       _path_with_metadata)))
  (:route_metadata
   (&rest __route_metadata)
   (if (keywordp (car __route_metadata))
       (send* _route_metadata __route_metadata)
     (progn
       (if __route_metadata (setq _route_metadata (car __route_metadata)))
       _route_metadata)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
    (apply #'+ (send-all _path_with_metadata :serialization-length)) 4
    ;; rtp_msgs/RouteMetadata _route_metadata
    (send _route_metadata :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
     (write-long (length _path_with_metadata) s)
     (dolist (elem _path_with_metadata)
       (send elem :serialize s)
       )
     ;; rtp_msgs/RouteMetadata _route_metadata
       (send _route_metadata :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; rtp_msgs/PathPointWithMetadata[] _path_with_metadata
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _path_with_metadata (let (r) (dotimes (i n) (push (instance rtp_msgs::PathPointWithMetadata :init) r)) r))
     (dolist (elem- _path_with_metadata)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;; rtp_msgs/RouteMetadata _route_metadata
     (send _route_metadata :deserialize buf ptr-) (incf ptr- (send _route_metadata :serialization-length))
   ;;
   self)
  )

(setf (get rtp_msgs::RouteTask :md5sum-) "ab34c3bd329118b09a519cb4febaeb24")
(setf (get rtp_msgs::RouteTask :datatype-) "rtp_msgs/RouteTask")
(setf (get rtp_msgs::RouteTask :definition-)
      "std_msgs/Header header

# Массив, содержащий точки маршрута с дополнительными свойствами
rtp_msgs/PathPointWithMetadata[] path_with_metadata

# Дополнительные свойства маршрутного задания
rtp_msgs/RouteMetadata route_metadata

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: rtp_msgs/PathPointWithMetadata
# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
geometry_msgs/Pose pose

# Дополнительные свойства точки маршрута/траектории
rtp_msgs/PathPointMetadata metadata
================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: rtp_msgs/PathPointMetadata
# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
================================================================================
MSG: rtp_msgs/RouteMetadata

# Тип маршрутного задания
uint8 route_type

# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней
# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно
# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно
# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение
uint8 ONE_WAY = 0
uint8 SHUTTLE_ONES = 1
uint8 SHUTTLE_LOOP = 2
uint8 LOOP = 3
")



(provide :rtp_msgs/RouteTask "ab34c3bd329118b09a519cb4febaeb24")


