;; Auto-generated. Do not edit!


(when (boundp 'rtp_msgs::RouteMetadata)
  (if (not (find-package "RTP_MSGS"))
    (make-package "RTP_MSGS"))
  (shadow 'RouteMetadata (find-package "RTP_MSGS")))
(unless (find-package "RTP_MSGS::ROUTEMETADATA")
  (make-package "RTP_MSGS::ROUTEMETADATA"))

(in-package "ROS")
;;//! \htmlinclude RouteMetadata.msg.html


(intern "*ONE_WAY*" (find-package "RTP_MSGS::ROUTEMETADATA"))
(shadow '*ONE_WAY* (find-package "RTP_MSGS::ROUTEMETADATA"))
(defconstant rtp_msgs::RouteMetadata::*ONE_WAY* 0)
(intern "*SHUTTLE_ONES*" (find-package "RTP_MSGS::ROUTEMETADATA"))
(shadow '*SHUTTLE_ONES* (find-package "RTP_MSGS::ROUTEMETADATA"))
(defconstant rtp_msgs::RouteMetadata::*SHUTTLE_ONES* 1)
(intern "*SHUTTLE_LOOP*" (find-package "RTP_MSGS::ROUTEMETADATA"))
(shadow '*SHUTTLE_LOOP* (find-package "RTP_MSGS::ROUTEMETADATA"))
(defconstant rtp_msgs::RouteMetadata::*SHUTTLE_LOOP* 2)
(intern "*LOOP*" (find-package "RTP_MSGS::ROUTEMETADATA"))
(shadow '*LOOP* (find-package "RTP_MSGS::ROUTEMETADATA"))
(defconstant rtp_msgs::RouteMetadata::*LOOP* 3)

(defun rtp_msgs::RouteMetadata-to-symbol (const)
  (cond
        ((= const 0) 'rtp_msgs::RouteMetadata::*ONE_WAY*)
        ((= const 1) 'rtp_msgs::RouteMetadata::*SHUTTLE_ONES*)
        ((= const 2) 'rtp_msgs::RouteMetadata::*SHUTTLE_LOOP*)
        ((= const 3) 'rtp_msgs::RouteMetadata::*LOOP*)
        (t nil)))

(defclass rtp_msgs::RouteMetadata
  :super ros::object
  :slots (_route_type ))

(defmethod rtp_msgs::RouteMetadata
  (:init
   (&key
    ((:route_type __route_type) 0)
    )
   (send-super :init)
   (setq _route_type (round __route_type))
   self)
  (:route_type
   (&optional __route_type)
   (if __route_type (setq _route_type __route_type)) _route_type)
  (:serialization-length
   ()
   (+
    ;; uint8 _route_type
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8 _route_type
       (write-byte _route_type s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8 _route_type
     (setq _route_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get rtp_msgs::RouteMetadata :md5sum-) "2e224d7e907791437e5f0065ca09eb50")
(setf (get rtp_msgs::RouteMetadata :datatype-) "rtp_msgs/RouteMetadata")
(setf (get rtp_msgs::RouteMetadata :definition-)
      "
# Тип маршрутного задания
uint8 route_type

# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней
# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно
# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно
# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение
uint8 ONE_WAY = 0
uint8 SHUTTLE_ONES = 1
uint8 SHUTTLE_LOOP = 2
uint8 LOOP = 3
")



(provide :rtp_msgs/RouteMetadata "2e224d7e907791437e5f0065ca09eb50")


