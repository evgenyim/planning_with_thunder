;; Auto-generated. Do not edit!


(when (boundp 'rtp_msgs::PathPointWithMetadata)
  (if (not (find-package "RTP_MSGS"))
    (make-package "RTP_MSGS"))
  (shadow 'PathPointWithMetadata (find-package "RTP_MSGS")))
(unless (find-package "RTP_MSGS::PATHPOINTWITHMETADATA")
  (make-package "RTP_MSGS::PATHPOINTWITHMETADATA"))

(in-package "ROS")
;;//! \htmlinclude PathPointWithMetadata.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))


(defclass rtp_msgs::PathPointWithMetadata
  :super ros::object
  :slots (_pose _metadata ))

(defmethod rtp_msgs::PathPointWithMetadata
  (:init
   (&key
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:metadata __metadata) (instance rtp_msgs::PathPointMetadata :init))
    )
   (send-super :init)
   (setq _pose __pose)
   (setq _metadata __metadata)
   self)
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:metadata
   (&rest __metadata)
   (if (keywordp (car __metadata))
       (send* _metadata __metadata)
     (progn
       (if __metadata (setq _metadata (car __metadata)))
       _metadata)))
  (:serialization-length
   ()
   (+
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; rtp_msgs/PathPointMetadata _metadata
    (send _metadata :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; rtp_msgs/PathPointMetadata _metadata
       (send _metadata :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; rtp_msgs/PathPointMetadata _metadata
     (send _metadata :deserialize buf ptr-) (incf ptr- (send _metadata :serialization-length))
   ;;
   self)
  )

(setf (get rtp_msgs::PathPointWithMetadata :md5sum-) "8183196cf0c6b777f94835553e444416")
(setf (get rtp_msgs::PathPointWithMetadata :datatype-) "rtp_msgs/PathPointWithMetadata")
(setf (get rtp_msgs::PathPointWithMetadata :definition-)
      "# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
geometry_msgs/Pose pose

# Дополнительные свойства точки маршрута/траектории
rtp_msgs/PathPointMetadata metadata
================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: rtp_msgs/PathPointMetadata
# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
")



(provide :rtp_msgs/PathPointWithMetadata "8183196cf0c6b777f94835553e444416")


