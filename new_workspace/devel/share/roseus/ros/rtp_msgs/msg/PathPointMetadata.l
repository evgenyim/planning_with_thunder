;; Auto-generated. Do not edit!


(when (boundp 'rtp_msgs::PathPointMetadata)
  (if (not (find-package "RTP_MSGS"))
    (make-package "RTP_MSGS"))
  (shadow 'PathPointMetadata (find-package "RTP_MSGS")))
(unless (find-package "RTP_MSGS::PATHPOINTMETADATA")
  (make-package "RTP_MSGS::PATHPOINTMETADATA"))

(in-package "ROS")
;;//! \htmlinclude PathPointMetadata.msg.html


(defclass rtp_msgs::PathPointMetadata
  :super ros::object
  :slots (_linear_velocity _max_deviation _key_point _return_point _confirmation_point _delay ))

(defmethod rtp_msgs::PathPointMetadata
  (:init
   (&key
    ((:linear_velocity __linear_velocity) 0.0)
    ((:max_deviation __max_deviation) 0.0)
    ((:key_point __key_point) nil)
    ((:return_point __return_point) nil)
    ((:confirmation_point __confirmation_point) nil)
    ((:delay __delay) 0.0)
    )
   (send-super :init)
   (setq _linear_velocity (float __linear_velocity))
   (setq _max_deviation (float __max_deviation))
   (setq _key_point __key_point)
   (setq _return_point __return_point)
   (setq _confirmation_point __confirmation_point)
   (setq _delay (float __delay))
   self)
  (:linear_velocity
   (&optional __linear_velocity)
   (if __linear_velocity (setq _linear_velocity __linear_velocity)) _linear_velocity)
  (:max_deviation
   (&optional __max_deviation)
   (if __max_deviation (setq _max_deviation __max_deviation)) _max_deviation)
  (:key_point
   (&optional (__key_point :null))
   (if (not (eq __key_point :null)) (setq _key_point __key_point)) _key_point)
  (:return_point
   (&optional (__return_point :null))
   (if (not (eq __return_point :null)) (setq _return_point __return_point)) _return_point)
  (:confirmation_point
   (&optional (__confirmation_point :null))
   (if (not (eq __confirmation_point :null)) (setq _confirmation_point __confirmation_point)) _confirmation_point)
  (:delay
   (&optional __delay)
   (if __delay (setq _delay __delay)) _delay)
  (:serialization-length
   ()
   (+
    ;; float32 _linear_velocity
    4
    ;; float32 _max_deviation
    4
    ;; bool _key_point
    1
    ;; bool _return_point
    1
    ;; bool _confirmation_point
    1
    ;; float32 _delay
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _linear_velocity
       (sys::poke _linear_velocity (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _max_deviation
       (sys::poke _max_deviation (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _key_point
       (if _key_point (write-byte -1 s) (write-byte 0 s))
     ;; bool _return_point
       (if _return_point (write-byte -1 s) (write-byte 0 s))
     ;; bool _confirmation_point
       (if _confirmation_point (write-byte -1 s) (write-byte 0 s))
     ;; float32 _delay
       (sys::poke _delay (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _linear_velocity
     (setq _linear_velocity (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _max_deviation
     (setq _max_deviation (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _key_point
     (setq _key_point (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _return_point
     (setq _return_point (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _confirmation_point
     (setq _confirmation_point (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float32 _delay
     (setq _delay (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get rtp_msgs::PathPointMetadata :md5sum-) "033df90186cc92c5bcba24a0c76bf8de")
(setf (get rtp_msgs::PathPointMetadata :datatype-) "rtp_msgs/PathPointMetadata")
(setf (get rtp_msgs::PathPointMetadata :definition-)
      "# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
")



(provide :rtp_msgs/PathPointMetadata "033df90186cc92c5bcba24a0c76bf8de")


