;; Auto-generated. Do not edit!


(when (boundp 'pnc_task_msgs::PlanningTask)
  (if (not (find-package "PNC_TASK_MSGS"))
    (make-package "PNC_TASK_MSGS"))
  (shadow 'PlanningTask (find-package "PNC_TASK_MSGS")))
(unless (find-package "PNC_TASK_MSGS::PLANNINGTASK")
  (make-package "PNC_TASK_MSGS::PLANNINGTASK"))

(in-package "ROS")
;;//! \htmlinclude PlanningTask.msg.html
(if (not (find-package "NAV_MSGS"))
  (ros::roseus-add-msgs "nav_msgs"))
(if (not (find-package "RTP_MSGS"))
  (ros::roseus-add-msgs "rtp_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass pnc_task_msgs::PlanningTask
  :super ros::object
  :slots (_header _init_point _goal_point _occ_grid _path_type _route_index_to _route_index_from ))

(defmethod pnc_task_msgs::PlanningTask
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:init_point __init_point) (instance rtp_msgs::PathPointWithMetadata :init))
    ((:goal_point __goal_point) (instance rtp_msgs::PathPointWithMetadata :init))
    ((:occ_grid __occ_grid) (instance nav_msgs::OccupancyGrid :init))
    ((:path_type __path_type) 0)
    ((:route_index_to __route_index_to) 0)
    ((:route_index_from __route_index_from) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _init_point __init_point)
   (setq _goal_point __goal_point)
   (setq _occ_grid __occ_grid)
   (setq _path_type (round __path_type))
   (setq _route_index_to (round __route_index_to))
   (setq _route_index_from (round __route_index_from))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:init_point
   (&rest __init_point)
   (if (keywordp (car __init_point))
       (send* _init_point __init_point)
     (progn
       (if __init_point (setq _init_point (car __init_point)))
       _init_point)))
  (:goal_point
   (&rest __goal_point)
   (if (keywordp (car __goal_point))
       (send* _goal_point __goal_point)
     (progn
       (if __goal_point (setq _goal_point (car __goal_point)))
       _goal_point)))
  (:occ_grid
   (&rest __occ_grid)
   (if (keywordp (car __occ_grid))
       (send* _occ_grid __occ_grid)
     (progn
       (if __occ_grid (setq _occ_grid (car __occ_grid)))
       _occ_grid)))
  (:path_type
   (&optional __path_type)
   (if __path_type (setq _path_type __path_type)) _path_type)
  (:route_index_to
   (&optional __route_index_to)
   (if __route_index_to (setq _route_index_to __route_index_to)) _route_index_to)
  (:route_index_from
   (&optional __route_index_from)
   (if __route_index_from (setq _route_index_from __route_index_from)) _route_index_from)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; rtp_msgs/PathPointWithMetadata _init_point
    (send _init_point :serialization-length)
    ;; rtp_msgs/PathPointWithMetadata _goal_point
    (send _goal_point :serialization-length)
    ;; nav_msgs/OccupancyGrid _occ_grid
    (send _occ_grid :serialization-length)
    ;; uint8 _path_type
    1
    ;; uint16 _route_index_to
    2
    ;; uint16 _route_index_from
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; rtp_msgs/PathPointWithMetadata _init_point
       (send _init_point :serialize s)
     ;; rtp_msgs/PathPointWithMetadata _goal_point
       (send _goal_point :serialize s)
     ;; nav_msgs/OccupancyGrid _occ_grid
       (send _occ_grid :serialize s)
     ;; uint8 _path_type
       (write-byte _path_type s)
     ;; uint16 _route_index_to
       (write-word _route_index_to s)
     ;; uint16 _route_index_from
       (write-word _route_index_from s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; rtp_msgs/PathPointWithMetadata _init_point
     (send _init_point :deserialize buf ptr-) (incf ptr- (send _init_point :serialization-length))
   ;; rtp_msgs/PathPointWithMetadata _goal_point
     (send _goal_point :deserialize buf ptr-) (incf ptr- (send _goal_point :serialization-length))
   ;; nav_msgs/OccupancyGrid _occ_grid
     (send _occ_grid :deserialize buf ptr-) (incf ptr- (send _occ_grid :serialization-length))
   ;; uint8 _path_type
     (setq _path_type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _route_index_to
     (setq _route_index_to (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _route_index_from
     (setq _route_index_from (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get pnc_task_msgs::PlanningTask :md5sum-) "fd40ef44d8eab5b0a4450060a66caae6")
(setf (get pnc_task_msgs::PlanningTask :datatype-) "pnc_task_msgs/PlanningTask")
(setf (get pnc_task_msgs::PlanningTask :definition-)
      "std_msgs/Header header

# Init robot position %
rtp_msgs/PathPointWithMetadata init_point

# Goal robot position %
rtp_msgs/PathPointWithMetadata goal_point

# Map %
nav_msgs/OccupancyGrid occ_grid

# Options %
uint8 path_type
uint16 route_index_to       # Индекс точки маршрута, к которой построена траектория
uint16 route_index_from     # Индекс точки маршрута, от которой построена траектория
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: rtp_msgs/PathPointWithMetadata
# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
geometry_msgs/Pose pose

# Дополнительные свойства точки маршрута/траектории
rtp_msgs/PathPointMetadata metadata
================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: rtp_msgs/PathPointMetadata
# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
================================================================================
MSG: nav_msgs/OccupancyGrid
# This represents a 2-D grid map, in which each cell represents the probability of
# occupancy.

Header header 

#MetaData for the map
MapMetaData info

# The map data, in row-major order, starting with (0,0).  Occupancy
# probabilities are in the range [0,100].  Unknown is -1.
int8[] data

================================================================================
MSG: nav_msgs/MapMetaData
# This hold basic information about the characterists of the OccupancyGrid

# The time at which the map was loaded
time map_load_time
# The map resolution [m/cell]
float32 resolution
# Map width [cells]
uint32 width
# Map height [cells]
uint32 height
# The origin of the map [m, m, rad].  This is the real-world pose of the
# cell (0,0) in the map.
geometry_msgs/Pose origin
")



(provide :pnc_task_msgs/PlanningTask "fd40ef44d8eab5b0a4450060a66caae6")


