;; Auto-generated. Do not edit!


(when (boundp 'pnc_task_msgs::ControlTask)
  (if (not (find-package "PNC_TASK_MSGS"))
    (make-package "PNC_TASK_MSGS"))
  (shadow 'ControlTask (find-package "PNC_TASK_MSGS")))
(unless (find-package "PNC_TASK_MSGS::CONTROLTASK")
  (make-package "PNC_TASK_MSGS::CONTROLTASK"))

(in-package "ROS")
;;//! \htmlinclude ControlTask.msg.html
(if (not (find-package "NAV_MSGS"))
  (ros::roseus-add-msgs "nav_msgs"))
(if (not (find-package "RTP_MSGS"))
  (ros::roseus-add-msgs "rtp_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass pnc_task_msgs::ControlTask
  :super ros::object
  :slots (_header _current_pose _estop _stop _trajectory _occ_grid ))

(defmethod pnc_task_msgs::ControlTask
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:current_pose __current_pose) (instance nav_msgs::Odometry :init))
    ((:estop __estop) nil)
    ((:stop __stop) nil)
    ((:trajectory __trajectory) (instance rtp_msgs::PathStamped :init))
    ((:occ_grid __occ_grid) (instance nav_msgs::OccupancyGrid :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _current_pose __current_pose)
   (setq _estop __estop)
   (setq _stop __stop)
   (setq _trajectory __trajectory)
   (setq _occ_grid __occ_grid)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:current_pose
   (&rest __current_pose)
   (if (keywordp (car __current_pose))
       (send* _current_pose __current_pose)
     (progn
       (if __current_pose (setq _current_pose (car __current_pose)))
       _current_pose)))
  (:estop
   (&optional (__estop :null))
   (if (not (eq __estop :null)) (setq _estop __estop)) _estop)
  (:stop
   (&optional (__stop :null))
   (if (not (eq __stop :null)) (setq _stop __stop)) _stop)
  (:trajectory
   (&rest __trajectory)
   (if (keywordp (car __trajectory))
       (send* _trajectory __trajectory)
     (progn
       (if __trajectory (setq _trajectory (car __trajectory)))
       _trajectory)))
  (:occ_grid
   (&rest __occ_grid)
   (if (keywordp (car __occ_grid))
       (send* _occ_grid __occ_grid)
     (progn
       (if __occ_grid (setq _occ_grid (car __occ_grid)))
       _occ_grid)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; nav_msgs/Odometry _current_pose
    (send _current_pose :serialization-length)
    ;; bool _estop
    1
    ;; bool _stop
    1
    ;; rtp_msgs/PathStamped _trajectory
    (send _trajectory :serialization-length)
    ;; nav_msgs/OccupancyGrid _occ_grid
    (send _occ_grid :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; nav_msgs/Odometry _current_pose
       (send _current_pose :serialize s)
     ;; bool _estop
       (if _estop (write-byte -1 s) (write-byte 0 s))
     ;; bool _stop
       (if _stop (write-byte -1 s) (write-byte 0 s))
     ;; rtp_msgs/PathStamped _trajectory
       (send _trajectory :serialize s)
     ;; nav_msgs/OccupancyGrid _occ_grid
       (send _occ_grid :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; nav_msgs/Odometry _current_pose
     (send _current_pose :deserialize buf ptr-) (incf ptr- (send _current_pose :serialization-length))
   ;; bool _estop
     (setq _estop (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _stop
     (setq _stop (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; rtp_msgs/PathStamped _trajectory
     (send _trajectory :deserialize buf ptr-) (incf ptr- (send _trajectory :serialization-length))
   ;; nav_msgs/OccupancyGrid _occ_grid
     (send _occ_grid :deserialize buf ptr-) (incf ptr- (send _occ_grid :serialization-length))
   ;;
   self)
  )

(setf (get pnc_task_msgs::ControlTask :md5sum-) "ce735c9485fcfbbb8078084d79a91cf2")
(setf (get pnc_task_msgs::ControlTask :datatype-) "pnc_task_msgs/ControlTask")
(setf (get pnc_task_msgs::ControlTask :definition-)
      "std_msgs/Header header

# Current robot position %
nav_msgs/Odometry current_pose

# Emergency stop %
bool estop

# Regular stop %
bool stop

# Trajectory %
rtp_msgs/PathStamped trajectory

# Map %
nav_msgs/OccupancyGrid occ_grid
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: nav_msgs/Odometry
# This represents an estimate of a position and velocity in free space.  
# The pose in this message should be specified in the coordinate frame given by header.frame_id.
# The twist in this message should be specified in the coordinate frame given by the child_frame_id
Header header
string child_frame_id
geometry_msgs/PoseWithCovariance pose
geometry_msgs/TwistWithCovariance twist

================================================================================
MSG: geometry_msgs/PoseWithCovariance
# This represents a pose in free space with uncertainty.

Pose pose

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
float64[36] covariance

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

================================================================================
MSG: geometry_msgs/TwistWithCovariance
# This expresses velocity in free space with uncertainty.

Twist twist

# Row-major representation of the 6x6 covariance matrix
# The orientation parameters use a fixed-axis representation.
# In order, the parameters are:
# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
float64[36] covariance

================================================================================
MSG: geometry_msgs/Twist
# This expresses velocity in free space broken into its linear and angular parts.
Vector3  linear
Vector3  angular

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
================================================================================
MSG: rtp_msgs/PathStamped
std_msgs/Header header

# Массив, содержащий точки траектории с дополнительными свойствами
rtp_msgs/PathPointWithMetadata[] path_with_metadata

# Тип траектории
uint8 path_type
uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве
uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса
uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа

# Индекс точки маршрута, к которой построена траектория
uint16 route_index_to

# Индекс точки маршрута, от которой построена траектория
uint16 route_index_from
================================================================================
MSG: rtp_msgs/PathPointWithMetadata
# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
geometry_msgs/Pose pose

# Дополнительные свойства точки маршрута/траектории
rtp_msgs/PathPointMetadata metadata
================================================================================
MSG: rtp_msgs/PathPointMetadata
# Значение максимальной линейной скорости движения на сегменте маршрута
float32 linear_velocity

# Значение максимально допустимого отклонения от заданного маршрута/траектории
float32 max_deviation

# Признак «ключевая точка»
bool key_point

# Признак «точка возврата»
bool return_point

# Признак «точка, требующая от оператора подтверждения продолжения движения»
bool confirmation_point

# Заданное время стоянки в точке
float32 delay
================================================================================
MSG: nav_msgs/OccupancyGrid
# This represents a 2-D grid map, in which each cell represents the probability of
# occupancy.

Header header 

#MetaData for the map
MapMetaData info

# The map data, in row-major order, starting with (0,0).  Occupancy
# probabilities are in the range [0,100].  Unknown is -1.
int8[] data

================================================================================
MSG: nav_msgs/MapMetaData
# This hold basic information about the characterists of the OccupancyGrid

# The time at which the map was loaded
time map_load_time
# The map resolution [m/cell]
float32 resolution
# Map width [cells]
uint32 width
# Map height [cells]
uint32 height
# The origin of the map [m, m, rad].  This is the real-world pose of the
# cell (0,0) in the map.
geometry_msgs/Pose origin
")



(provide :pnc_task_msgs/ControlTask "ce735c9485fcfbbb8078084d79a91cf2")


