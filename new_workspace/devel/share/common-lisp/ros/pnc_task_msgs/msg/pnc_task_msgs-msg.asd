
(cl:in-package :asdf)

(defsystem "pnc_task_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :nav_msgs-msg
               :rtp_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "ControlTask" :depends-on ("_package_ControlTask"))
    (:file "_package_ControlTask" :depends-on ("_package"))
    (:file "PlanningTask" :depends-on ("_package_PlanningTask"))
    (:file "_package_PlanningTask" :depends-on ("_package"))
  ))