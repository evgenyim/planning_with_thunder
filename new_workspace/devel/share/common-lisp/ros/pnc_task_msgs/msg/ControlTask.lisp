; Auto-generated. Do not edit!


(cl:in-package pnc_task_msgs-msg)


;//! \htmlinclude ControlTask.msg.html

(cl:defclass <ControlTask> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (current_pose
    :reader current_pose
    :initarg :current_pose
    :type nav_msgs-msg:Odometry
    :initform (cl:make-instance 'nav_msgs-msg:Odometry))
   (estop
    :reader estop
    :initarg :estop
    :type cl:boolean
    :initform cl:nil)
   (stop
    :reader stop
    :initarg :stop
    :type cl:boolean
    :initform cl:nil)
   (trajectory
    :reader trajectory
    :initarg :trajectory
    :type rtp_msgs-msg:PathStamped
    :initform (cl:make-instance 'rtp_msgs-msg:PathStamped))
   (occ_grid
    :reader occ_grid
    :initarg :occ_grid
    :type nav_msgs-msg:OccupancyGrid
    :initform (cl:make-instance 'nav_msgs-msg:OccupancyGrid)))
)

(cl:defclass ControlTask (<ControlTask>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ControlTask>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ControlTask)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pnc_task_msgs-msg:<ControlTask> is deprecated: use pnc_task_msgs-msg:ControlTask instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:header-val is deprecated.  Use pnc_task_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'current_pose-val :lambda-list '(m))
(cl:defmethod current_pose-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:current_pose-val is deprecated.  Use pnc_task_msgs-msg:current_pose instead.")
  (current_pose m))

(cl:ensure-generic-function 'estop-val :lambda-list '(m))
(cl:defmethod estop-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:estop-val is deprecated.  Use pnc_task_msgs-msg:estop instead.")
  (estop m))

(cl:ensure-generic-function 'stop-val :lambda-list '(m))
(cl:defmethod stop-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:stop-val is deprecated.  Use pnc_task_msgs-msg:stop instead.")
  (stop m))

(cl:ensure-generic-function 'trajectory-val :lambda-list '(m))
(cl:defmethod trajectory-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:trajectory-val is deprecated.  Use pnc_task_msgs-msg:trajectory instead.")
  (trajectory m))

(cl:ensure-generic-function 'occ_grid-val :lambda-list '(m))
(cl:defmethod occ_grid-val ((m <ControlTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:occ_grid-val is deprecated.  Use pnc_task_msgs-msg:occ_grid instead.")
  (occ_grid m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ControlTask>) ostream)
  "Serializes a message object of type '<ControlTask>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'current_pose) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'estop) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'stop) 1 0)) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'trajectory) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'occ_grid) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ControlTask>) istream)
  "Deserializes a message object of type '<ControlTask>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'current_pose) istream)
    (cl:setf (cl:slot-value msg 'estop) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'stop) (cl:not (cl:zerop (cl:read-byte istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'trajectory) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'occ_grid) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ControlTask>)))
  "Returns string type for a message object of type '<ControlTask>"
  "pnc_task_msgs/ControlTask")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ControlTask)))
  "Returns string type for a message object of type 'ControlTask"
  "pnc_task_msgs/ControlTask")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ControlTask>)))
  "Returns md5sum for a message object of type '<ControlTask>"
  "ce735c9485fcfbbb8078084d79a91cf2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ControlTask)))
  "Returns md5sum for a message object of type 'ControlTask"
  "ce735c9485fcfbbb8078084d79a91cf2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ControlTask>)))
  "Returns full string definition for message of type '<ControlTask>"
  (cl:format cl:nil "std_msgs/Header header~%~%# Current robot position %~%nav_msgs/Odometry current_pose~%~%# Emergency stop %~%bool estop~%~%# Regular stop %~%bool stop~%~%# Trajectory %~%rtp_msgs/PathStamped trajectory~%~%# Map %~%nav_msgs/OccupancyGrid occ_grid~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: nav_msgs/Odometry~%# This represents an estimate of a position and velocity in free space.  ~%# The pose in this message should be specified in the coordinate frame given by header.frame_id.~%# The twist in this message should be specified in the coordinate frame given by the child_frame_id~%Header header~%string child_frame_id~%geometry_msgs/PoseWithCovariance pose~%geometry_msgs/TwistWithCovariance twist~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/TwistWithCovariance~%# This expresses velocity in free space with uncertainty.~%~%Twist twist~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: rtp_msgs/PathStamped~%std_msgs/Header header~%~%# Массив, содержащий точки траектории с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Тип траектории~%uint8 path_type~%uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве~%uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса~%uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа~%~%# Индекс точки маршрута, к которой построена траектория~%uint16 route_index_to~%~%# Индекс точки маршрута, от которой построена траектория~%uint16 route_index_from~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: nav_msgs/OccupancyGrid~%# This represents a 2-D grid map, in which each cell represents the probability of~%# occupancy.~%~%Header header ~%~%#MetaData for the map~%MapMetaData info~%~%# The map data, in row-major order, starting with (0,0).  Occupancy~%# probabilities are in the range [0,100].  Unknown is -1.~%int8[] data~%~%================================================================================~%MSG: nav_msgs/MapMetaData~%# This hold basic information about the characterists of the OccupancyGrid~%~%# The time at which the map was loaded~%time map_load_time~%# The map resolution [m/cell]~%float32 resolution~%# Map width [cells]~%uint32 width~%# Map height [cells]~%uint32 height~%# The origin of the map [m, m, rad].  This is the real-world pose of the~%# cell (0,0) in the map.~%geometry_msgs/Pose origin~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ControlTask)))
  "Returns full string definition for message of type 'ControlTask"
  (cl:format cl:nil "std_msgs/Header header~%~%# Current robot position %~%nav_msgs/Odometry current_pose~%~%# Emergency stop %~%bool estop~%~%# Regular stop %~%bool stop~%~%# Trajectory %~%rtp_msgs/PathStamped trajectory~%~%# Map %~%nav_msgs/OccupancyGrid occ_grid~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: nav_msgs/Odometry~%# This represents an estimate of a position and velocity in free space.  ~%# The pose in this message should be specified in the coordinate frame given by header.frame_id.~%# The twist in this message should be specified in the coordinate frame given by the child_frame_id~%Header header~%string child_frame_id~%geometry_msgs/PoseWithCovariance pose~%geometry_msgs/TwistWithCovariance twist~%~%================================================================================~%MSG: geometry_msgs/PoseWithCovariance~%# This represents a pose in free space with uncertainty.~%~%Pose pose~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: geometry_msgs/TwistWithCovariance~%# This expresses velocity in free space with uncertainty.~%~%Twist twist~%~%# Row-major representation of the 6x6 covariance matrix~%# The orientation parameters use a fixed-axis representation.~%# In order, the parameters are:~%# (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)~%float64[36] covariance~%~%================================================================================~%MSG: geometry_msgs/Twist~%# This expresses velocity in free space broken into its linear and angular parts.~%Vector3  linear~%Vector3  angular~%~%================================================================================~%MSG: geometry_msgs/Vector3~%# This represents a vector in free space. ~%# It is only meant to represent a direction. Therefore, it does not~%# make sense to apply a translation to it (e.g., when applying a ~%# generic rigid transformation to a Vector3, tf2 will only apply the~%# rotation). If you want your data to be translatable too, use the~%# geometry_msgs/Point message instead.~%~%float64 x~%float64 y~%float64 z~%================================================================================~%MSG: rtp_msgs/PathStamped~%std_msgs/Header header~%~%# Массив, содержащий точки траектории с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Тип траектории~%uint8 path_type~%uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве~%uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса~%uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа~%~%# Индекс точки маршрута, к которой построена траектория~%uint16 route_index_to~%~%# Индекс точки маршрута, от которой построена траектория~%uint16 route_index_from~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: nav_msgs/OccupancyGrid~%# This represents a 2-D grid map, in which each cell represents the probability of~%# occupancy.~%~%Header header ~%~%#MetaData for the map~%MapMetaData info~%~%# The map data, in row-major order, starting with (0,0).  Occupancy~%# probabilities are in the range [0,100].  Unknown is -1.~%int8[] data~%~%================================================================================~%MSG: nav_msgs/MapMetaData~%# This hold basic information about the characterists of the OccupancyGrid~%~%# The time at which the map was loaded~%time map_load_time~%# The map resolution [m/cell]~%float32 resolution~%# Map width [cells]~%uint32 width~%# Map height [cells]~%uint32 height~%# The origin of the map [m, m, rad].  This is the real-world pose of the~%# cell (0,0) in the map.~%geometry_msgs/Pose origin~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ControlTask>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'current_pose))
     1
     1
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'trajectory))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'occ_grid))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ControlTask>))
  "Converts a ROS message object to a list"
  (cl:list 'ControlTask
    (cl:cons ':header (header msg))
    (cl:cons ':current_pose (current_pose msg))
    (cl:cons ':estop (estop msg))
    (cl:cons ':stop (stop msg))
    (cl:cons ':trajectory (trajectory msg))
    (cl:cons ':occ_grid (occ_grid msg))
))
