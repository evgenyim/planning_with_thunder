(cl:in-package pnc_task_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          CURRENT_POSE-VAL
          CURRENT_POSE
          ESTOP-VAL
          ESTOP
          STOP-VAL
          STOP
          TRAJECTORY-VAL
          TRAJECTORY
          OCC_GRID-VAL
          OCC_GRID
))