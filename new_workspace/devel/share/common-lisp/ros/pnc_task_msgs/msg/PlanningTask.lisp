; Auto-generated. Do not edit!


(cl:in-package pnc_task_msgs-msg)


;//! \htmlinclude PlanningTask.msg.html

(cl:defclass <PlanningTask> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (init_point
    :reader init_point
    :initarg :init_point
    :type rtp_msgs-msg:PathPointWithMetadata
    :initform (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata))
   (goal_point
    :reader goal_point
    :initarg :goal_point
    :type rtp_msgs-msg:PathPointWithMetadata
    :initform (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata))
   (occ_grid
    :reader occ_grid
    :initarg :occ_grid
    :type nav_msgs-msg:OccupancyGrid
    :initform (cl:make-instance 'nav_msgs-msg:OccupancyGrid))
   (path_type
    :reader path_type
    :initarg :path_type
    :type cl:fixnum
    :initform 0)
   (route_index_to
    :reader route_index_to
    :initarg :route_index_to
    :type cl:fixnum
    :initform 0)
   (route_index_from
    :reader route_index_from
    :initarg :route_index_from
    :type cl:fixnum
    :initform 0))
)

(cl:defclass PlanningTask (<PlanningTask>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PlanningTask>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PlanningTask)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pnc_task_msgs-msg:<PlanningTask> is deprecated: use pnc_task_msgs-msg:PlanningTask instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:header-val is deprecated.  Use pnc_task_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'init_point-val :lambda-list '(m))
(cl:defmethod init_point-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:init_point-val is deprecated.  Use pnc_task_msgs-msg:init_point instead.")
  (init_point m))

(cl:ensure-generic-function 'goal_point-val :lambda-list '(m))
(cl:defmethod goal_point-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:goal_point-val is deprecated.  Use pnc_task_msgs-msg:goal_point instead.")
  (goal_point m))

(cl:ensure-generic-function 'occ_grid-val :lambda-list '(m))
(cl:defmethod occ_grid-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:occ_grid-val is deprecated.  Use pnc_task_msgs-msg:occ_grid instead.")
  (occ_grid m))

(cl:ensure-generic-function 'path_type-val :lambda-list '(m))
(cl:defmethod path_type-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:path_type-val is deprecated.  Use pnc_task_msgs-msg:path_type instead.")
  (path_type m))

(cl:ensure-generic-function 'route_index_to-val :lambda-list '(m))
(cl:defmethod route_index_to-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:route_index_to-val is deprecated.  Use pnc_task_msgs-msg:route_index_to instead.")
  (route_index_to m))

(cl:ensure-generic-function 'route_index_from-val :lambda-list '(m))
(cl:defmethod route_index_from-val ((m <PlanningTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pnc_task_msgs-msg:route_index_from-val is deprecated.  Use pnc_task_msgs-msg:route_index_from instead.")
  (route_index_from m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PlanningTask>) ostream)
  "Serializes a message object of type '<PlanningTask>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'init_point) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'goal_point) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'occ_grid) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_to)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_to)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_from)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_from)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PlanningTask>) istream)
  "Deserializes a message object of type '<PlanningTask>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'init_point) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'goal_point) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'occ_grid) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_to)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_to)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_from)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_from)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PlanningTask>)))
  "Returns string type for a message object of type '<PlanningTask>"
  "pnc_task_msgs/PlanningTask")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PlanningTask)))
  "Returns string type for a message object of type 'PlanningTask"
  "pnc_task_msgs/PlanningTask")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PlanningTask>)))
  "Returns md5sum for a message object of type '<PlanningTask>"
  "fd40ef44d8eab5b0a4450060a66caae6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PlanningTask)))
  "Returns md5sum for a message object of type 'PlanningTask"
  "fd40ef44d8eab5b0a4450060a66caae6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PlanningTask>)))
  "Returns full string definition for message of type '<PlanningTask>"
  (cl:format cl:nil "std_msgs/Header header~%~%# Init robot position %~%rtp_msgs/PathPointWithMetadata init_point~%~%# Goal robot position %~%rtp_msgs/PathPointWithMetadata goal_point~%~%# Map %~%nav_msgs/OccupancyGrid occ_grid~%~%# Options %~%uint8 path_type~%uint16 route_index_to       # Индекс точки маршрута, к которой построена траектория~%uint16 route_index_from     # Индекс точки маршрута, от которой построена траектория~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: nav_msgs/OccupancyGrid~%# This represents a 2-D grid map, in which each cell represents the probability of~%# occupancy.~%~%Header header ~%~%#MetaData for the map~%MapMetaData info~%~%# The map data, in row-major order, starting with (0,0).  Occupancy~%# probabilities are in the range [0,100].  Unknown is -1.~%int8[] data~%~%================================================================================~%MSG: nav_msgs/MapMetaData~%# This hold basic information about the characterists of the OccupancyGrid~%~%# The time at which the map was loaded~%time map_load_time~%# The map resolution [m/cell]~%float32 resolution~%# Map width [cells]~%uint32 width~%# Map height [cells]~%uint32 height~%# The origin of the map [m, m, rad].  This is the real-world pose of the~%# cell (0,0) in the map.~%geometry_msgs/Pose origin~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PlanningTask)))
  "Returns full string definition for message of type 'PlanningTask"
  (cl:format cl:nil "std_msgs/Header header~%~%# Init robot position %~%rtp_msgs/PathPointWithMetadata init_point~%~%# Goal robot position %~%rtp_msgs/PathPointWithMetadata goal_point~%~%# Map %~%nav_msgs/OccupancyGrid occ_grid~%~%# Options %~%uint8 path_type~%uint16 route_index_to       # Индекс точки маршрута, к которой построена траектория~%uint16 route_index_from     # Индекс точки маршрута, от которой построена траектория~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: nav_msgs/OccupancyGrid~%# This represents a 2-D grid map, in which each cell represents the probability of~%# occupancy.~%~%Header header ~%~%#MetaData for the map~%MapMetaData info~%~%# The map data, in row-major order, starting with (0,0).  Occupancy~%# probabilities are in the range [0,100].  Unknown is -1.~%int8[] data~%~%================================================================================~%MSG: nav_msgs/MapMetaData~%# This hold basic information about the characterists of the OccupancyGrid~%~%# The time at which the map was loaded~%time map_load_time~%# The map resolution [m/cell]~%float32 resolution~%# Map width [cells]~%uint32 width~%# Map height [cells]~%uint32 height~%# The origin of the map [m, m, rad].  This is the real-world pose of the~%# cell (0,0) in the map.~%geometry_msgs/Pose origin~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PlanningTask>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'init_point))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'goal_point))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'occ_grid))
     1
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PlanningTask>))
  "Converts a ROS message object to a list"
  (cl:list 'PlanningTask
    (cl:cons ':header (header msg))
    (cl:cons ':init_point (init_point msg))
    (cl:cons ':goal_point (goal_point msg))
    (cl:cons ':occ_grid (occ_grid msg))
    (cl:cons ':path_type (path_type msg))
    (cl:cons ':route_index_to (route_index_to msg))
    (cl:cons ':route_index_from (route_index_from msg))
))
