(cl:in-package pnc_task_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          INIT_POINT-VAL
          INIT_POINT
          GOAL_POINT-VAL
          GOAL_POINT
          OCC_GRID-VAL
          OCC_GRID
          PATH_TYPE-VAL
          PATH_TYPE
          ROUTE_INDEX_TO-VAL
          ROUTE_INDEX_TO
          ROUTE_INDEX_FROM-VAL
          ROUTE_INDEX_FROM
))