
(cl:in-package :asdf)

(defsystem "thunder_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "ThunderControl" :depends-on ("_package_ThunderControl"))
    (:file "_package_ThunderControl" :depends-on ("_package"))
  ))