; Auto-generated. Do not edit!


(cl:in-package thunder_msgs-msg)


;//! \htmlinclude ThunderControl.msg.html

(cl:defclass <ThunderControl> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (can_id
    :reader can_id
    :initarg :can_id
    :type cl:integer
    :initform 0)
   (control_mode
    :reader control_mode
    :initarg :control_mode
    :type cl:fixnum
    :initform 0)
   (fan_pump
    :reader fan_pump
    :initarg :fan_pump
    :type cl:fixnum
    :initform 0)
   (control_switch
    :reader control_switch
    :initarg :control_switch
    :type cl:fixnum
    :initform 0)
   (ptz_switch
    :reader ptz_switch
    :initarg :ptz_switch
    :type cl:fixnum
    :initform 0)
   (ptz
    :reader ptz
    :initarg :ptz
    :type cl:fixnum
    :initform 0))
)

(cl:defclass ThunderControl (<ThunderControl>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <ThunderControl>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'ThunderControl)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name thunder_msgs-msg:<ThunderControl> is deprecated: use thunder_msgs-msg:ThunderControl instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:header-val is deprecated.  Use thunder_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'can_id-val :lambda-list '(m))
(cl:defmethod can_id-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:can_id-val is deprecated.  Use thunder_msgs-msg:can_id instead.")
  (can_id m))

(cl:ensure-generic-function 'control_mode-val :lambda-list '(m))
(cl:defmethod control_mode-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:control_mode-val is deprecated.  Use thunder_msgs-msg:control_mode instead.")
  (control_mode m))

(cl:ensure-generic-function 'fan_pump-val :lambda-list '(m))
(cl:defmethod fan_pump-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:fan_pump-val is deprecated.  Use thunder_msgs-msg:fan_pump instead.")
  (fan_pump m))

(cl:ensure-generic-function 'control_switch-val :lambda-list '(m))
(cl:defmethod control_switch-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:control_switch-val is deprecated.  Use thunder_msgs-msg:control_switch instead.")
  (control_switch m))

(cl:ensure-generic-function 'ptz_switch-val :lambda-list '(m))
(cl:defmethod ptz_switch-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:ptz_switch-val is deprecated.  Use thunder_msgs-msg:ptz_switch instead.")
  (ptz_switch m))

(cl:ensure-generic-function 'ptz-val :lambda-list '(m))
(cl:defmethod ptz-val ((m <ThunderControl>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thunder_msgs-msg:ptz-val is deprecated.  Use thunder_msgs-msg:ptz instead.")
  (ptz m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<ThunderControl>)))
    "Constants for message type '<ThunderControl>"
  '((:CAN_CONTROL_ID . 528)
    (:CAN_PTZ_ID . 353)
    (:CONTROL_MODE_NONE . 10)
    (:CONTROL_MODE_FAN . 11)
    (:CONTROL_MODE_AUTO . 12)
    (:CONTROL_MODE_PTZ . 13)
    (:FAN_PUMP_NONE . 20)
    (:FAN_PUMP_CLOSE . 22)
    (:FAN_PUMP_OPEN . 23)
    (:CONTROL_SWITCH_NONE . 30)
    (:CONTROL_SWITCH_CONTROL . 32)
    (:CONTROL_SWITCH_PTZ . 31)
    (:PTZ_SWITCH_NONE . 40)
    (:PTZ_SWITCH_MANUAL . 42)
    (:PTZ_SWITCH_AUTO . 43))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'ThunderControl)))
    "Constants for message type 'ThunderControl"
  '((:CAN_CONTROL_ID . 528)
    (:CAN_PTZ_ID . 353)
    (:CONTROL_MODE_NONE . 10)
    (:CONTROL_MODE_FAN . 11)
    (:CONTROL_MODE_AUTO . 12)
    (:CONTROL_MODE_PTZ . 13)
    (:FAN_PUMP_NONE . 20)
    (:FAN_PUMP_CLOSE . 22)
    (:FAN_PUMP_OPEN . 23)
    (:CONTROL_SWITCH_NONE . 30)
    (:CONTROL_SWITCH_CONTROL . 32)
    (:CONTROL_SWITCH_PTZ . 31)
    (:PTZ_SWITCH_NONE . 40)
    (:PTZ_SWITCH_MANUAL . 42)
    (:PTZ_SWITCH_AUTO . 43))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <ThunderControl>) ostream)
  "Serializes a message object of type '<ThunderControl>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'can_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'can_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'can_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'can_id)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'control_mode)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'fan_pump)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'control_switch)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ptz_switch)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'ptz)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <ThunderControl>) istream)
  "Deserializes a message object of type '<ThunderControl>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'can_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'can_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'can_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'can_id)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'control_mode)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'fan_pump)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'control_switch)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'ptz_switch)) (cl:read-byte istream))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'ptz) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<ThunderControl>)))
  "Returns string type for a message object of type '<ThunderControl>"
  "thunder_msgs/ThunderControl")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'ThunderControl)))
  "Returns string type for a message object of type 'ThunderControl"
  "thunder_msgs/ThunderControl")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<ThunderControl>)))
  "Returns md5sum for a message object of type '<ThunderControl>"
  "5e455a4d42a93ecb6aa73b9ba635c80a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'ThunderControl)))
  "Returns md5sum for a message object of type 'ThunderControl"
  "5e455a4d42a93ecb6aa73b9ba635c80a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<ThunderControl>)))
  "Returns full string definition for message of type '<ThunderControl>"
  (cl:format cl:nil "Header header~%~%uint32 CAN_CONTROL_ID = 528     # 0x210~%uint32 CAN_PTZ_ID = 353         # 0x161 ~%~%uint8 CONTROL_MODE_NONE = 10~%uint8 CONTROL_MODE_FAN = 11~%uint8 CONTROL_MODE_AUTO = 12~%uint8 CONTROL_MODE_PTZ = 13~%~%uint8 FAN_PUMP_NONE = 20~%uint8 FAN_PUMP_CLOSE = 22~%uint8 FAN_PUMP_OPEN = 23~%~%uint8 CONTROL_SWITCH_NONE = 30~%uint8 CONTROL_SWITCH_CONTROL = 32~%uint8 CONTROL_SWITCH_PTZ = 31~%~%uint8 PTZ_SWITCH_NONE = 40~%uint8 PTZ_SWITCH_MANUAL = 42~%uint8 PTZ_SWITCH_AUTO = 43~%~%# type of can message~%uint32 can_id~%~%# control command~%uint8 control_mode~%~%# ptz command~%uint8 fan_pump~%uint8 control_switch~%uint8 ptz_switch~%int16 ptz~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'ThunderControl)))
  "Returns full string definition for message of type 'ThunderControl"
  (cl:format cl:nil "Header header~%~%uint32 CAN_CONTROL_ID = 528     # 0x210~%uint32 CAN_PTZ_ID = 353         # 0x161 ~%~%uint8 CONTROL_MODE_NONE = 10~%uint8 CONTROL_MODE_FAN = 11~%uint8 CONTROL_MODE_AUTO = 12~%uint8 CONTROL_MODE_PTZ = 13~%~%uint8 FAN_PUMP_NONE = 20~%uint8 FAN_PUMP_CLOSE = 22~%uint8 FAN_PUMP_OPEN = 23~%~%uint8 CONTROL_SWITCH_NONE = 30~%uint8 CONTROL_SWITCH_CONTROL = 32~%uint8 CONTROL_SWITCH_PTZ = 31~%~%uint8 PTZ_SWITCH_NONE = 40~%uint8 PTZ_SWITCH_MANUAL = 42~%uint8 PTZ_SWITCH_AUTO = 43~%~%# type of can message~%uint32 can_id~%~%# control command~%uint8 control_mode~%~%# ptz command~%uint8 fan_pump~%uint8 control_switch~%uint8 ptz_switch~%int16 ptz~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <ThunderControl>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     1
     1
     1
     1
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <ThunderControl>))
  "Converts a ROS message object to a list"
  (cl:list 'ThunderControl
    (cl:cons ':header (header msg))
    (cl:cons ':can_id (can_id msg))
    (cl:cons ':control_mode (control_mode msg))
    (cl:cons ':fan_pump (fan_pump msg))
    (cl:cons ':control_switch (control_switch msg))
    (cl:cons ':ptz_switch (ptz_switch msg))
    (cl:cons ':ptz (ptz msg))
))
