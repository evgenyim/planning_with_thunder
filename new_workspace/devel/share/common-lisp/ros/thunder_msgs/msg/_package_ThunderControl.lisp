(cl:in-package thunder_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          CAN_ID-VAL
          CAN_ID
          CONTROL_MODE-VAL
          CONTROL_MODE
          FAN_PUMP-VAL
          FAN_PUMP
          CONTROL_SWITCH-VAL
          CONTROL_SWITCH
          PTZ_SWITCH-VAL
          PTZ_SWITCH
          PTZ-VAL
          PTZ
))