; Auto-generated. Do not edit!


(cl:in-package rtp_msgs-msg)


;//! \htmlinclude PathStamped.msg.html

(cl:defclass <PathStamped> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (path_with_metadata
    :reader path_with_metadata
    :initarg :path_with_metadata
    :type (cl:vector rtp_msgs-msg:PathPointWithMetadata)
   :initform (cl:make-array 0 :element-type 'rtp_msgs-msg:PathPointWithMetadata :initial-element (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata)))
   (path_type
    :reader path_type
    :initarg :path_type
    :type cl:fixnum
    :initform 0)
   (route_index_to
    :reader route_index_to
    :initarg :route_index_to
    :type cl:fixnum
    :initform 0)
   (route_index_from
    :reader route_index_from
    :initarg :route_index_from
    :type cl:fixnum
    :initform 0))
)

(cl:defclass PathStamped (<PathStamped>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathStamped>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathStamped)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rtp_msgs-msg:<PathStamped> is deprecated: use rtp_msgs-msg:PathStamped instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PathStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:header-val is deprecated.  Use rtp_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'path_with_metadata-val :lambda-list '(m))
(cl:defmethod path_with_metadata-val ((m <PathStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:path_with_metadata-val is deprecated.  Use rtp_msgs-msg:path_with_metadata instead.")
  (path_with_metadata m))

(cl:ensure-generic-function 'path_type-val :lambda-list '(m))
(cl:defmethod path_type-val ((m <PathStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:path_type-val is deprecated.  Use rtp_msgs-msg:path_type instead.")
  (path_type m))

(cl:ensure-generic-function 'route_index_to-val :lambda-list '(m))
(cl:defmethod route_index_to-val ((m <PathStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:route_index_to-val is deprecated.  Use rtp_msgs-msg:route_index_to instead.")
  (route_index_to m))

(cl:ensure-generic-function 'route_index_from-val :lambda-list '(m))
(cl:defmethod route_index_from-val ((m <PathStamped>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:route_index_from-val is deprecated.  Use rtp_msgs-msg:route_index_from instead.")
  (route_index_from m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<PathStamped>)))
    "Constants for message type '<PathStamped>"
  '((:EUCLIDEAN . 0)
    (:DUBINS . 1)
    (:REEDS_SHEPP . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'PathStamped)))
    "Constants for message type 'PathStamped"
  '((:EUCLIDEAN . 0)
    (:DUBINS . 1)
    (:REEDS_SHEPP . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathStamped>) ostream)
  "Serializes a message object of type '<PathStamped>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'path_with_metadata))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'path_with_metadata))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_to)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_to)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_from)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_from)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathStamped>) istream)
  "Deserializes a message object of type '<PathStamped>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'path_with_metadata) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'path_with_metadata)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'path_type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_to)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_to)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_index_from)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'route_index_from)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathStamped>)))
  "Returns string type for a message object of type '<PathStamped>"
  "rtp_msgs/PathStamped")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathStamped)))
  "Returns string type for a message object of type 'PathStamped"
  "rtp_msgs/PathStamped")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathStamped>)))
  "Returns md5sum for a message object of type '<PathStamped>"
  "ebd42ddb68da2c8327f5578deb75232f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathStamped)))
  "Returns md5sum for a message object of type 'PathStamped"
  "ebd42ddb68da2c8327f5578deb75232f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathStamped>)))
  "Returns full string definition for message of type '<PathStamped>"
  (cl:format cl:nil "std_msgs/Header header~%~%# Массив, содержащий точки траектории с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Тип траектории~%uint8 path_type~%uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве~%uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса~%uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа~%~%# Индекс точки маршрута, к которой построена траектория~%uint16 route_index_to~%~%# Индекс точки маршрута, от которой построена траектория~%uint16 route_index_from~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathStamped)))
  "Returns full string definition for message of type 'PathStamped"
  (cl:format cl:nil "std_msgs/Header header~%~%# Массив, содержащий точки траектории с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Тип траектории~%uint8 path_type~%uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве~%uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса~%uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа~%~%# Индекс точки маршрута, к которой построена траектория~%uint16 route_index_to~%~%# Индекс точки маршрута, от которой построена траектория~%uint16 route_index_from~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathStamped>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'path_with_metadata) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     1
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathStamped>))
  "Converts a ROS message object to a list"
  (cl:list 'PathStamped
    (cl:cons ':header (header msg))
    (cl:cons ':path_with_metadata (path_with_metadata msg))
    (cl:cons ':path_type (path_type msg))
    (cl:cons ':route_index_to (route_index_to msg))
    (cl:cons ':route_index_from (route_index_from msg))
))
