
(cl:in-package :asdf)

(defsystem "rtp_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "PathPointMetadata" :depends-on ("_package_PathPointMetadata"))
    (:file "_package_PathPointMetadata" :depends-on ("_package"))
    (:file "PathPointWithMetadata" :depends-on ("_package_PathPointWithMetadata"))
    (:file "_package_PathPointWithMetadata" :depends-on ("_package"))
    (:file "PathStamped" :depends-on ("_package_PathStamped"))
    (:file "_package_PathStamped" :depends-on ("_package"))
    (:file "RouteMetadata" :depends-on ("_package_RouteMetadata"))
    (:file "_package_RouteMetadata" :depends-on ("_package"))
    (:file "RouteTask" :depends-on ("_package_RouteTask"))
    (:file "_package_RouteTask" :depends-on ("_package"))
  ))