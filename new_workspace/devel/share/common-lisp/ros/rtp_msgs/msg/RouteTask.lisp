; Auto-generated. Do not edit!


(cl:in-package rtp_msgs-msg)


;//! \htmlinclude RouteTask.msg.html

(cl:defclass <RouteTask> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (path_with_metadata
    :reader path_with_metadata
    :initarg :path_with_metadata
    :type (cl:vector rtp_msgs-msg:PathPointWithMetadata)
   :initform (cl:make-array 0 :element-type 'rtp_msgs-msg:PathPointWithMetadata :initial-element (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata)))
   (route_metadata
    :reader route_metadata
    :initarg :route_metadata
    :type rtp_msgs-msg:RouteMetadata
    :initform (cl:make-instance 'rtp_msgs-msg:RouteMetadata)))
)

(cl:defclass RouteTask (<RouteTask>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RouteTask>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RouteTask)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rtp_msgs-msg:<RouteTask> is deprecated: use rtp_msgs-msg:RouteTask instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <RouteTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:header-val is deprecated.  Use rtp_msgs-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'path_with_metadata-val :lambda-list '(m))
(cl:defmethod path_with_metadata-val ((m <RouteTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:path_with_metadata-val is deprecated.  Use rtp_msgs-msg:path_with_metadata instead.")
  (path_with_metadata m))

(cl:ensure-generic-function 'route_metadata-val :lambda-list '(m))
(cl:defmethod route_metadata-val ((m <RouteTask>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:route_metadata-val is deprecated.  Use rtp_msgs-msg:route_metadata instead.")
  (route_metadata m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RouteTask>) ostream)
  "Serializes a message object of type '<RouteTask>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'path_with_metadata))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'path_with_metadata))
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'route_metadata) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RouteTask>) istream)
  "Deserializes a message object of type '<RouteTask>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'path_with_metadata) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'path_with_metadata)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'rtp_msgs-msg:PathPointWithMetadata))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'route_metadata) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RouteTask>)))
  "Returns string type for a message object of type '<RouteTask>"
  "rtp_msgs/RouteTask")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RouteTask)))
  "Returns string type for a message object of type 'RouteTask"
  "rtp_msgs/RouteTask")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RouteTask>)))
  "Returns md5sum for a message object of type '<RouteTask>"
  "ab34c3bd329118b09a519cb4febaeb24")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RouteTask)))
  "Returns md5sum for a message object of type 'RouteTask"
  "ab34c3bd329118b09a519cb4febaeb24")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RouteTask>)))
  "Returns full string definition for message of type '<RouteTask>"
  (cl:format cl:nil "std_msgs/Header header~%~%# Массив, содержащий точки маршрута с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Дополнительные свойства маршрутного задания~%rtp_msgs/RouteMetadata route_metadata~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: rtp_msgs/RouteMetadata~%~%# Тип маршрутного задания~%uint8 route_type~%~%# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней~%# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно~%# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно~%# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение~%uint8 ONE_WAY = 0~%uint8 SHUTTLE_ONES = 1~%uint8 SHUTTLE_LOOP = 2~%uint8 LOOP = 3~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RouteTask)))
  "Returns full string definition for message of type 'RouteTask"
  (cl:format cl:nil "std_msgs/Header header~%~%# Массив, содержащий точки маршрута с дополнительными свойствами~%rtp_msgs/PathPointWithMetadata[] path_with_metadata~%~%# Дополнительные свойства маршрутного задания~%rtp_msgs/RouteMetadata route_metadata~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: rtp_msgs/PathPointWithMetadata~%# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%================================================================================~%MSG: rtp_msgs/RouteMetadata~%~%# Тип маршрутного задания~%uint8 route_type~%~%# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней~%# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно~%# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно~%# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение~%uint8 ONE_WAY = 0~%uint8 SHUTTLE_ONES = 1~%uint8 SHUTTLE_LOOP = 2~%uint8 LOOP = 3~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RouteTask>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'path_with_metadata) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'route_metadata))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RouteTask>))
  "Converts a ROS message object to a list"
  (cl:list 'RouteTask
    (cl:cons ':header (header msg))
    (cl:cons ':path_with_metadata (path_with_metadata msg))
    (cl:cons ':route_metadata (route_metadata msg))
))
