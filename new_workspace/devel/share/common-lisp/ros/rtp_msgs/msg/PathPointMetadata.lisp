; Auto-generated. Do not edit!


(cl:in-package rtp_msgs-msg)


;//! \htmlinclude PathPointMetadata.msg.html

(cl:defclass <PathPointMetadata> (roslisp-msg-protocol:ros-message)
  ((linear_velocity
    :reader linear_velocity
    :initarg :linear_velocity
    :type cl:float
    :initform 0.0)
   (max_deviation
    :reader max_deviation
    :initarg :max_deviation
    :type cl:float
    :initform 0.0)
   (key_point
    :reader key_point
    :initarg :key_point
    :type cl:boolean
    :initform cl:nil)
   (return_point
    :reader return_point
    :initarg :return_point
    :type cl:boolean
    :initform cl:nil)
   (confirmation_point
    :reader confirmation_point
    :initarg :confirmation_point
    :type cl:boolean
    :initform cl:nil)
   (delay
    :reader delay
    :initarg :delay
    :type cl:float
    :initform 0.0))
)

(cl:defclass PathPointMetadata (<PathPointMetadata>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathPointMetadata>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathPointMetadata)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rtp_msgs-msg:<PathPointMetadata> is deprecated: use rtp_msgs-msg:PathPointMetadata instead.")))

(cl:ensure-generic-function 'linear_velocity-val :lambda-list '(m))
(cl:defmethod linear_velocity-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:linear_velocity-val is deprecated.  Use rtp_msgs-msg:linear_velocity instead.")
  (linear_velocity m))

(cl:ensure-generic-function 'max_deviation-val :lambda-list '(m))
(cl:defmethod max_deviation-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:max_deviation-val is deprecated.  Use rtp_msgs-msg:max_deviation instead.")
  (max_deviation m))

(cl:ensure-generic-function 'key_point-val :lambda-list '(m))
(cl:defmethod key_point-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:key_point-val is deprecated.  Use rtp_msgs-msg:key_point instead.")
  (key_point m))

(cl:ensure-generic-function 'return_point-val :lambda-list '(m))
(cl:defmethod return_point-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:return_point-val is deprecated.  Use rtp_msgs-msg:return_point instead.")
  (return_point m))

(cl:ensure-generic-function 'confirmation_point-val :lambda-list '(m))
(cl:defmethod confirmation_point-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:confirmation_point-val is deprecated.  Use rtp_msgs-msg:confirmation_point instead.")
  (confirmation_point m))

(cl:ensure-generic-function 'delay-val :lambda-list '(m))
(cl:defmethod delay-val ((m <PathPointMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:delay-val is deprecated.  Use rtp_msgs-msg:delay instead.")
  (delay m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathPointMetadata>) ostream)
  "Serializes a message object of type '<PathPointMetadata>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'linear_velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'max_deviation))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'key_point) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'return_point) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'confirmation_point) 1 0)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'delay))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathPointMetadata>) istream)
  "Deserializes a message object of type '<PathPointMetadata>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'linear_velocity) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'max_deviation) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'key_point) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'return_point) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'confirmation_point) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'delay) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathPointMetadata>)))
  "Returns string type for a message object of type '<PathPointMetadata>"
  "rtp_msgs/PathPointMetadata")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathPointMetadata)))
  "Returns string type for a message object of type 'PathPointMetadata"
  "rtp_msgs/PathPointMetadata")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathPointMetadata>)))
  "Returns md5sum for a message object of type '<PathPointMetadata>"
  "033df90186cc92c5bcba24a0c76bf8de")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathPointMetadata)))
  "Returns md5sum for a message object of type 'PathPointMetadata"
  "033df90186cc92c5bcba24a0c76bf8de")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathPointMetadata>)))
  "Returns full string definition for message of type '<PathPointMetadata>"
  (cl:format cl:nil "# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathPointMetadata)))
  "Returns full string definition for message of type 'PathPointMetadata"
  (cl:format cl:nil "# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathPointMetadata>))
  (cl:+ 0
     4
     4
     1
     1
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathPointMetadata>))
  "Converts a ROS message object to a list"
  (cl:list 'PathPointMetadata
    (cl:cons ':linear_velocity (linear_velocity msg))
    (cl:cons ':max_deviation (max_deviation msg))
    (cl:cons ':key_point (key_point msg))
    (cl:cons ':return_point (return_point msg))
    (cl:cons ':confirmation_point (confirmation_point msg))
    (cl:cons ':delay (delay msg))
))
