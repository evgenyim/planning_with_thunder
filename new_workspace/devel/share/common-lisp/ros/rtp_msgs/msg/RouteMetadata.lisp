; Auto-generated. Do not edit!


(cl:in-package rtp_msgs-msg)


;//! \htmlinclude RouteMetadata.msg.html

(cl:defclass <RouteMetadata> (roslisp-msg-protocol:ros-message)
  ((route_type
    :reader route_type
    :initarg :route_type
    :type cl:fixnum
    :initform 0))
)

(cl:defclass RouteMetadata (<RouteMetadata>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RouteMetadata>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RouteMetadata)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rtp_msgs-msg:<RouteMetadata> is deprecated: use rtp_msgs-msg:RouteMetadata instead.")))

(cl:ensure-generic-function 'route_type-val :lambda-list '(m))
(cl:defmethod route_type-val ((m <RouteMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:route_type-val is deprecated.  Use rtp_msgs-msg:route_type instead.")
  (route_type m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<RouteMetadata>)))
    "Constants for message type '<RouteMetadata>"
  '((:ONE_WAY . 0)
    (:SHUTTLE_ONES . 1)
    (:SHUTTLE_LOOP . 2)
    (:LOOP . 3))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'RouteMetadata)))
    "Constants for message type 'RouteMetadata"
  '((:ONE_WAY . 0)
    (:SHUTTLE_ONES . 1)
    (:SHUTTLE_LOOP . 2)
    (:LOOP . 3))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RouteMetadata>) ostream)
  "Serializes a message object of type '<RouteMetadata>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_type)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RouteMetadata>) istream)
  "Deserializes a message object of type '<RouteMetadata>"
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'route_type)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RouteMetadata>)))
  "Returns string type for a message object of type '<RouteMetadata>"
  "rtp_msgs/RouteMetadata")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RouteMetadata)))
  "Returns string type for a message object of type 'RouteMetadata"
  "rtp_msgs/RouteMetadata")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RouteMetadata>)))
  "Returns md5sum for a message object of type '<RouteMetadata>"
  "2e224d7e907791437e5f0065ca09eb50")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RouteMetadata)))
  "Returns md5sum for a message object of type 'RouteMetadata"
  "2e224d7e907791437e5f0065ca09eb50")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RouteMetadata>)))
  "Returns full string definition for message of type '<RouteMetadata>"
  (cl:format cl:nil "~%# Тип маршрутного задания~%uint8 route_type~%~%# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней~%# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно~%# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно~%# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение~%uint8 ONE_WAY = 0~%uint8 SHUTTLE_ONES = 1~%uint8 SHUTTLE_LOOP = 2~%uint8 LOOP = 3~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RouteMetadata)))
  "Returns full string definition for message of type 'RouteMetadata"
  (cl:format cl:nil "~%# Тип маршрутного задания~%uint8 route_type~%~%# ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней~%# SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно~%# SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно~%# LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение~%uint8 ONE_WAY = 0~%uint8 SHUTTLE_ONES = 1~%uint8 SHUTTLE_LOOP = 2~%uint8 LOOP = 3~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RouteMetadata>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RouteMetadata>))
  "Converts a ROS message object to a list"
  (cl:list 'RouteMetadata
    (cl:cons ':route_type (route_type msg))
))
