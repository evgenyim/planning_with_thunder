; Auto-generated. Do not edit!


(cl:in-package rtp_msgs-msg)


;//! \htmlinclude PathPointWithMetadata.msg.html

(cl:defclass <PathPointWithMetadata> (roslisp-msg-protocol:ros-message)
  ((pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose))
   (metadata
    :reader metadata
    :initarg :metadata
    :type rtp_msgs-msg:PathPointMetadata
    :initform (cl:make-instance 'rtp_msgs-msg:PathPointMetadata)))
)

(cl:defclass PathPointWithMetadata (<PathPointWithMetadata>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PathPointWithMetadata>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PathPointWithMetadata)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rtp_msgs-msg:<PathPointWithMetadata> is deprecated: use rtp_msgs-msg:PathPointWithMetadata instead.")))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <PathPointWithMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:pose-val is deprecated.  Use rtp_msgs-msg:pose instead.")
  (pose m))

(cl:ensure-generic-function 'metadata-val :lambda-list '(m))
(cl:defmethod metadata-val ((m <PathPointWithMetadata>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rtp_msgs-msg:metadata-val is deprecated.  Use rtp_msgs-msg:metadata instead.")
  (metadata m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PathPointWithMetadata>) ostream)
  "Serializes a message object of type '<PathPointWithMetadata>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'metadata) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PathPointWithMetadata>) istream)
  "Deserializes a message object of type '<PathPointWithMetadata>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'metadata) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PathPointWithMetadata>)))
  "Returns string type for a message object of type '<PathPointWithMetadata>"
  "rtp_msgs/PathPointWithMetadata")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PathPointWithMetadata)))
  "Returns string type for a message object of type 'PathPointWithMetadata"
  "rtp_msgs/PathPointWithMetadata")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PathPointWithMetadata>)))
  "Returns md5sum for a message object of type '<PathPointWithMetadata>"
  "8183196cf0c6b777f94835553e444416")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PathPointWithMetadata)))
  "Returns md5sum for a message object of type 'PathPointWithMetadata"
  "8183196cf0c6b777f94835553e444416")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PathPointWithMetadata>)))
  "Returns full string definition for message of type '<PathPointWithMetadata>"
  (cl:format cl:nil "# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PathPointWithMetadata)))
  "Returns full string definition for message of type 'PathPointWithMetadata"
  (cl:format cl:nil "# Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)~%geometry_msgs/Pose pose~%~%# Дополнительные свойства точки маршрута/траектории~%rtp_msgs/PathPointMetadata metadata~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%================================================================================~%MSG: rtp_msgs/PathPointMetadata~%# Значение максимальной линейной скорости движения на сегменте маршрута~%float32 linear_velocity~%~%# Значение максимально допустимого отклонения от заданного маршрута/траектории~%float32 max_deviation~%~%# Признак «ключевая точка»~%bool key_point~%~%# Признак «точка возврата»~%bool return_point~%~%# Признак «точка, требующая от оператора подтверждения продолжения движения»~%bool confirmation_point~%~%# Заданное время стоянки в точке~%float32 delay~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PathPointWithMetadata>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'metadata))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PathPointWithMetadata>))
  "Converts a ROS message object to a list"
  (cl:list 'PathPointWithMetadata
    (cl:cons ':pose (pose msg))
    (cl:cons ':metadata (metadata msg))
))
