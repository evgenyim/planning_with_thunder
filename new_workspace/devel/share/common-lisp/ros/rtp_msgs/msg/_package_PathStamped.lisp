(cl:in-package rtp_msgs-msg)
(cl:export '(HEADER-VAL
          HEADER
          PATH_WITH_METADATA-VAL
          PATH_WITH_METADATA
          PATH_TYPE-VAL
          PATH_TYPE
          ROUTE_INDEX_TO-VAL
          ROUTE_INDEX_TO
          ROUTE_INDEX_FROM-VAL
          ROUTE_INDEX_FROM
))