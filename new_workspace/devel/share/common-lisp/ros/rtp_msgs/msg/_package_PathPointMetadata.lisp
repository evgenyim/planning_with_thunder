(cl:in-package rtp_msgs-msg)
(cl:export '(LINEAR_VELOCITY-VAL
          LINEAR_VELOCITY
          MAX_DEVIATION-VAL
          MAX_DEVIATION
          KEY_POINT-VAL
          KEY_POINT
          RETURN_POINT-VAL
          RETURN_POINT
          CONFIRMATION_POINT-VAL
          CONFIRMATION_POINT
          DELAY-VAL
          DELAY
))