// Auto-generated. Do not edit!

// (in-package thunder_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class ThunderControl {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.can_id = null;
      this.control_mode = null;
      this.fan_pump = null;
      this.control_switch = null;
      this.ptz_switch = null;
      this.ptz = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('can_id')) {
        this.can_id = initObj.can_id
      }
      else {
        this.can_id = 0;
      }
      if (initObj.hasOwnProperty('control_mode')) {
        this.control_mode = initObj.control_mode
      }
      else {
        this.control_mode = 0;
      }
      if (initObj.hasOwnProperty('fan_pump')) {
        this.fan_pump = initObj.fan_pump
      }
      else {
        this.fan_pump = 0;
      }
      if (initObj.hasOwnProperty('control_switch')) {
        this.control_switch = initObj.control_switch
      }
      else {
        this.control_switch = 0;
      }
      if (initObj.hasOwnProperty('ptz_switch')) {
        this.ptz_switch = initObj.ptz_switch
      }
      else {
        this.ptz_switch = 0;
      }
      if (initObj.hasOwnProperty('ptz')) {
        this.ptz = initObj.ptz
      }
      else {
        this.ptz = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ThunderControl
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [can_id]
    bufferOffset = _serializer.uint32(obj.can_id, buffer, bufferOffset);
    // Serialize message field [control_mode]
    bufferOffset = _serializer.uint8(obj.control_mode, buffer, bufferOffset);
    // Serialize message field [fan_pump]
    bufferOffset = _serializer.uint8(obj.fan_pump, buffer, bufferOffset);
    // Serialize message field [control_switch]
    bufferOffset = _serializer.uint8(obj.control_switch, buffer, bufferOffset);
    // Serialize message field [ptz_switch]
    bufferOffset = _serializer.uint8(obj.ptz_switch, buffer, bufferOffset);
    // Serialize message field [ptz]
    bufferOffset = _serializer.int16(obj.ptz, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ThunderControl
    let len;
    let data = new ThunderControl(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [can_id]
    data.can_id = _deserializer.uint32(buffer, bufferOffset);
    // Deserialize message field [control_mode]
    data.control_mode = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [fan_pump]
    data.fan_pump = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [control_switch]
    data.control_switch = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [ptz_switch]
    data.ptz_switch = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [ptz]
    data.ptz = _deserializer.int16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 10;
  }

  static datatype() {
    // Returns string type for a message object
    return 'thunder_msgs/ThunderControl';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5e455a4d42a93ecb6aa73b9ba635c80a';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    
    uint32 CAN_CONTROL_ID = 528     # 0x210
    uint32 CAN_PTZ_ID = 353         # 0x161 
    
    uint8 CONTROL_MODE_NONE = 10
    uint8 CONTROL_MODE_FAN = 11
    uint8 CONTROL_MODE_AUTO = 12
    uint8 CONTROL_MODE_PTZ = 13
    
    uint8 FAN_PUMP_NONE = 20
    uint8 FAN_PUMP_CLOSE = 22
    uint8 FAN_PUMP_OPEN = 23
    
    uint8 CONTROL_SWITCH_NONE = 30
    uint8 CONTROL_SWITCH_CONTROL = 32
    uint8 CONTROL_SWITCH_PTZ = 31
    
    uint8 PTZ_SWITCH_NONE = 40
    uint8 PTZ_SWITCH_MANUAL = 42
    uint8 PTZ_SWITCH_AUTO = 43
    
    # type of can message
    uint32 can_id
    
    # control command
    uint8 control_mode
    
    # ptz command
    uint8 fan_pump
    uint8 control_switch
    uint8 ptz_switch
    int16 ptz
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ThunderControl(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.can_id !== undefined) {
      resolved.can_id = msg.can_id;
    }
    else {
      resolved.can_id = 0
    }

    if (msg.control_mode !== undefined) {
      resolved.control_mode = msg.control_mode;
    }
    else {
      resolved.control_mode = 0
    }

    if (msg.fan_pump !== undefined) {
      resolved.fan_pump = msg.fan_pump;
    }
    else {
      resolved.fan_pump = 0
    }

    if (msg.control_switch !== undefined) {
      resolved.control_switch = msg.control_switch;
    }
    else {
      resolved.control_switch = 0
    }

    if (msg.ptz_switch !== undefined) {
      resolved.ptz_switch = msg.ptz_switch;
    }
    else {
      resolved.ptz_switch = 0
    }

    if (msg.ptz !== undefined) {
      resolved.ptz = msg.ptz;
    }
    else {
      resolved.ptz = 0
    }

    return resolved;
    }
};

// Constants for message
ThunderControl.Constants = {
  CAN_CONTROL_ID: 528,
  CAN_PTZ_ID: 353,
  CONTROL_MODE_NONE: 10,
  CONTROL_MODE_FAN: 11,
  CONTROL_MODE_AUTO: 12,
  CONTROL_MODE_PTZ: 13,
  FAN_PUMP_NONE: 20,
  FAN_PUMP_CLOSE: 22,
  FAN_PUMP_OPEN: 23,
  CONTROL_SWITCH_NONE: 30,
  CONTROL_SWITCH_CONTROL: 32,
  CONTROL_SWITCH_PTZ: 31,
  PTZ_SWITCH_NONE: 40,
  PTZ_SWITCH_MANUAL: 42,
  PTZ_SWITCH_AUTO: 43,
}

module.exports = ThunderControl;
