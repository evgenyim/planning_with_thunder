// Auto-generated. Do not edit!

// (in-package rtp_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let PathPointMetadata = require('./PathPointMetadata.js');
let geometry_msgs = _finder('geometry_msgs');

//-----------------------------------------------------------

class PathPointWithMetadata {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.pose = null;
      this.metadata = null;
    }
    else {
      if (initObj.hasOwnProperty('pose')) {
        this.pose = initObj.pose
      }
      else {
        this.pose = new geometry_msgs.msg.Pose();
      }
      if (initObj.hasOwnProperty('metadata')) {
        this.metadata = initObj.metadata
      }
      else {
        this.metadata = new PathPointMetadata();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathPointWithMetadata
    // Serialize message field [pose]
    bufferOffset = geometry_msgs.msg.Pose.serialize(obj.pose, buffer, bufferOffset);
    // Serialize message field [metadata]
    bufferOffset = PathPointMetadata.serialize(obj.metadata, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathPointWithMetadata
    let len;
    let data = new PathPointWithMetadata(null);
    // Deserialize message field [pose]
    data.pose = geometry_msgs.msg.Pose.deserialize(buffer, bufferOffset);
    // Deserialize message field [metadata]
    data.metadata = PathPointMetadata.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 71;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rtp_msgs/PathPointWithMetadata';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8183196cf0c6b777f94835553e444416';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
    geometry_msgs/Pose pose
    
    # Дополнительные свойства точки маршрута/траектории
    rtp_msgs/PathPointMetadata metadata
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: rtp_msgs/PathPointMetadata
    # Значение максимальной линейной скорости движения на сегменте маршрута
    float32 linear_velocity
    
    # Значение максимально допустимого отклонения от заданного маршрута/траектории
    float32 max_deviation
    
    # Признак «ключевая точка»
    bool key_point
    
    # Признак «точка возврата»
    bool return_point
    
    # Признак «точка, требующая от оператора подтверждения продолжения движения»
    bool confirmation_point
    
    # Заданное время стоянки в точке
    float32 delay
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathPointWithMetadata(null);
    if (msg.pose !== undefined) {
      resolved.pose = geometry_msgs.msg.Pose.Resolve(msg.pose)
    }
    else {
      resolved.pose = new geometry_msgs.msg.Pose()
    }

    if (msg.metadata !== undefined) {
      resolved.metadata = PathPointMetadata.Resolve(msg.metadata)
    }
    else {
      resolved.metadata = new PathPointMetadata()
    }

    return resolved;
    }
};

module.exports = PathPointWithMetadata;
