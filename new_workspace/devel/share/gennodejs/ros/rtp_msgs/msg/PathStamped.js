// Auto-generated. Do not edit!

// (in-package rtp_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let PathPointWithMetadata = require('./PathPointWithMetadata.js');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class PathStamped {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.path_with_metadata = null;
      this.path_type = null;
      this.route_index_to = null;
      this.route_index_from = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('path_with_metadata')) {
        this.path_with_metadata = initObj.path_with_metadata
      }
      else {
        this.path_with_metadata = [];
      }
      if (initObj.hasOwnProperty('path_type')) {
        this.path_type = initObj.path_type
      }
      else {
        this.path_type = 0;
      }
      if (initObj.hasOwnProperty('route_index_to')) {
        this.route_index_to = initObj.route_index_to
      }
      else {
        this.route_index_to = 0;
      }
      if (initObj.hasOwnProperty('route_index_from')) {
        this.route_index_from = initObj.route_index_from
      }
      else {
        this.route_index_from = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathStamped
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [path_with_metadata]
    // Serialize the length for message field [path_with_metadata]
    bufferOffset = _serializer.uint32(obj.path_with_metadata.length, buffer, bufferOffset);
    obj.path_with_metadata.forEach((val) => {
      bufferOffset = PathPointWithMetadata.serialize(val, buffer, bufferOffset);
    });
    // Serialize message field [path_type]
    bufferOffset = _serializer.uint8(obj.path_type, buffer, bufferOffset);
    // Serialize message field [route_index_to]
    bufferOffset = _serializer.uint16(obj.route_index_to, buffer, bufferOffset);
    // Serialize message field [route_index_from]
    bufferOffset = _serializer.uint16(obj.route_index_from, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathStamped
    let len;
    let data = new PathStamped(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [path_with_metadata]
    // Deserialize array length for message field [path_with_metadata]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.path_with_metadata = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.path_with_metadata[i] = PathPointWithMetadata.deserialize(buffer, bufferOffset)
    }
    // Deserialize message field [path_type]
    data.path_type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [route_index_to]
    data.route_index_to = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [route_index_from]
    data.route_index_from = _deserializer.uint16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += 71 * object.path_with_metadata.length;
    return length + 9;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rtp_msgs/PathStamped';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ebd42ddb68da2c8327f5578deb75232f';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    
    # Массив, содержащий точки траектории с дополнительными свойствами
    rtp_msgs/PathPointWithMetadata[] path_with_metadata
    
    # Тип траектории
    uint8 path_type
    uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве
    uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса
    uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа
    
    # Индекс точки маршрута, к которой построена траектория
    uint16 route_index_to
    
    # Индекс точки маршрута, от которой построена траектория
    uint16 route_index_from
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: rtp_msgs/PathPointWithMetadata
    # Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
    geometry_msgs/Pose pose
    
    # Дополнительные свойства точки маршрута/траектории
    rtp_msgs/PathPointMetadata metadata
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: rtp_msgs/PathPointMetadata
    # Значение максимальной линейной скорости движения на сегменте маршрута
    float32 linear_velocity
    
    # Значение максимально допустимого отклонения от заданного маршрута/траектории
    float32 max_deviation
    
    # Признак «ключевая точка»
    bool key_point
    
    # Признак «точка возврата»
    bool return_point
    
    # Признак «точка, требующая от оператора подтверждения продолжения движения»
    bool confirmation_point
    
    # Заданное время стоянки в точке
    float32 delay
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathStamped(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.path_with_metadata !== undefined) {
      resolved.path_with_metadata = new Array(msg.path_with_metadata.length);
      for (let i = 0; i < resolved.path_with_metadata.length; ++i) {
        resolved.path_with_metadata[i] = PathPointWithMetadata.Resolve(msg.path_with_metadata[i]);
      }
    }
    else {
      resolved.path_with_metadata = []
    }

    if (msg.path_type !== undefined) {
      resolved.path_type = msg.path_type;
    }
    else {
      resolved.path_type = 0
    }

    if (msg.route_index_to !== undefined) {
      resolved.route_index_to = msg.route_index_to;
    }
    else {
      resolved.route_index_to = 0
    }

    if (msg.route_index_from !== undefined) {
      resolved.route_index_from = msg.route_index_from;
    }
    else {
      resolved.route_index_from = 0
    }

    return resolved;
    }
};

// Constants for message
PathStamped.Constants = {
  EUCLIDEAN: 0,
  DUBINS: 1,
  REEDS_SHEPP: 2,
}

module.exports = PathStamped;
