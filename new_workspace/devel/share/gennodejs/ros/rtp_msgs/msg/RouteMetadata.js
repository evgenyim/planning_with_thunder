// Auto-generated. Do not edit!

// (in-package rtp_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class RouteMetadata {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.route_type = null;
    }
    else {
      if (initObj.hasOwnProperty('route_type')) {
        this.route_type = initObj.route_type
      }
      else {
        this.route_type = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RouteMetadata
    // Serialize message field [route_type]
    bufferOffset = _serializer.uint8(obj.route_type, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RouteMetadata
    let len;
    let data = new RouteMetadata(null);
    // Deserialize message field [route_type]
    data.route_type = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rtp_msgs/RouteMetadata';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2e224d7e907791437e5f0065ca09eb50';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    # Тип маршрутного задания
    uint8 route_type
    
    # ONE_WAY = 0                Признак, определяющий односторонний тип маршрутного задания: последовательное однократное движение по ключевым точкам от первой к последней
    # SHUTTLE_ONES = 1           Признак, определяющий челночный тип маршрутного задания: последовательное однократное движение от первой точки к конечной и обратно
    # SHUTTLE_LOOP = 2           Признак, определяющий зацикленный челночный тип маршрутного задания: последовательное многократное движение от первой точки к конечной и обратно
    # LOOP = 3                   Признак, определяющий циклический тип маршрутного задания: последовательное движение от первой точки к последней, затем сразу переход в первую точку, многократное повторение
    uint8 ONE_WAY = 0
    uint8 SHUTTLE_ONES = 1
    uint8 SHUTTLE_LOOP = 2
    uint8 LOOP = 3
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RouteMetadata(null);
    if (msg.route_type !== undefined) {
      resolved.route_type = msg.route_type;
    }
    else {
      resolved.route_type = 0
    }

    return resolved;
    }
};

// Constants for message
RouteMetadata.Constants = {
  ONE_WAY: 0,
  SHUTTLE_ONES: 1,
  SHUTTLE_LOOP: 2,
  LOOP: 3,
}

module.exports = RouteMetadata;
