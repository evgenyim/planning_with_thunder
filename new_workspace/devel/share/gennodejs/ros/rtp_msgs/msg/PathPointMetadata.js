// Auto-generated. Do not edit!

// (in-package rtp_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class PathPointMetadata {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.linear_velocity = null;
      this.max_deviation = null;
      this.key_point = null;
      this.return_point = null;
      this.confirmation_point = null;
      this.delay = null;
    }
    else {
      if (initObj.hasOwnProperty('linear_velocity')) {
        this.linear_velocity = initObj.linear_velocity
      }
      else {
        this.linear_velocity = 0.0;
      }
      if (initObj.hasOwnProperty('max_deviation')) {
        this.max_deviation = initObj.max_deviation
      }
      else {
        this.max_deviation = 0.0;
      }
      if (initObj.hasOwnProperty('key_point')) {
        this.key_point = initObj.key_point
      }
      else {
        this.key_point = false;
      }
      if (initObj.hasOwnProperty('return_point')) {
        this.return_point = initObj.return_point
      }
      else {
        this.return_point = false;
      }
      if (initObj.hasOwnProperty('confirmation_point')) {
        this.confirmation_point = initObj.confirmation_point
      }
      else {
        this.confirmation_point = false;
      }
      if (initObj.hasOwnProperty('delay')) {
        this.delay = initObj.delay
      }
      else {
        this.delay = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PathPointMetadata
    // Serialize message field [linear_velocity]
    bufferOffset = _serializer.float32(obj.linear_velocity, buffer, bufferOffset);
    // Serialize message field [max_deviation]
    bufferOffset = _serializer.float32(obj.max_deviation, buffer, bufferOffset);
    // Serialize message field [key_point]
    bufferOffset = _serializer.bool(obj.key_point, buffer, bufferOffset);
    // Serialize message field [return_point]
    bufferOffset = _serializer.bool(obj.return_point, buffer, bufferOffset);
    // Serialize message field [confirmation_point]
    bufferOffset = _serializer.bool(obj.confirmation_point, buffer, bufferOffset);
    // Serialize message field [delay]
    bufferOffset = _serializer.float32(obj.delay, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PathPointMetadata
    let len;
    let data = new PathPointMetadata(null);
    // Deserialize message field [linear_velocity]
    data.linear_velocity = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [max_deviation]
    data.max_deviation = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [key_point]
    data.key_point = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [return_point]
    data.return_point = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [confirmation_point]
    data.confirmation_point = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [delay]
    data.delay = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 15;
  }

  static datatype() {
    // Returns string type for a message object
    return 'rtp_msgs/PathPointMetadata';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '033df90186cc92c5bcba24a0c76bf8de';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # Значение максимальной линейной скорости движения на сегменте маршрута
    float32 linear_velocity
    
    # Значение максимально допустимого отклонения от заданного маршрута/траектории
    float32 max_deviation
    
    # Признак «ключевая точка»
    bool key_point
    
    # Признак «точка возврата»
    bool return_point
    
    # Признак «точка, требующая от оператора подтверждения продолжения движения»
    bool confirmation_point
    
    # Заданное время стоянки в точке
    float32 delay
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PathPointMetadata(null);
    if (msg.linear_velocity !== undefined) {
      resolved.linear_velocity = msg.linear_velocity;
    }
    else {
      resolved.linear_velocity = 0.0
    }

    if (msg.max_deviation !== undefined) {
      resolved.max_deviation = msg.max_deviation;
    }
    else {
      resolved.max_deviation = 0.0
    }

    if (msg.key_point !== undefined) {
      resolved.key_point = msg.key_point;
    }
    else {
      resolved.key_point = false
    }

    if (msg.return_point !== undefined) {
      resolved.return_point = msg.return_point;
    }
    else {
      resolved.return_point = false
    }

    if (msg.confirmation_point !== undefined) {
      resolved.confirmation_point = msg.confirmation_point;
    }
    else {
      resolved.confirmation_point = false
    }

    if (msg.delay !== undefined) {
      resolved.delay = msg.delay;
    }
    else {
      resolved.delay = 0.0
    }

    return resolved;
    }
};

module.exports = PathPointMetadata;
