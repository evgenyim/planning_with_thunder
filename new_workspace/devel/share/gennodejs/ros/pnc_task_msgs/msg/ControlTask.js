// Auto-generated. Do not edit!

// (in-package pnc_task_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let rtp_msgs = _finder('rtp_msgs');
let nav_msgs = _finder('nav_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class ControlTask {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.current_pose = null;
      this.estop = null;
      this.stop = null;
      this.trajectory = null;
      this.occ_grid = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('current_pose')) {
        this.current_pose = initObj.current_pose
      }
      else {
        this.current_pose = new nav_msgs.msg.Odometry();
      }
      if (initObj.hasOwnProperty('estop')) {
        this.estop = initObj.estop
      }
      else {
        this.estop = false;
      }
      if (initObj.hasOwnProperty('stop')) {
        this.stop = initObj.stop
      }
      else {
        this.stop = false;
      }
      if (initObj.hasOwnProperty('trajectory')) {
        this.trajectory = initObj.trajectory
      }
      else {
        this.trajectory = new rtp_msgs.msg.PathStamped();
      }
      if (initObj.hasOwnProperty('occ_grid')) {
        this.occ_grid = initObj.occ_grid
      }
      else {
        this.occ_grid = new nav_msgs.msg.OccupancyGrid();
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type ControlTask
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [current_pose]
    bufferOffset = nav_msgs.msg.Odometry.serialize(obj.current_pose, buffer, bufferOffset);
    // Serialize message field [estop]
    bufferOffset = _serializer.bool(obj.estop, buffer, bufferOffset);
    // Serialize message field [stop]
    bufferOffset = _serializer.bool(obj.stop, buffer, bufferOffset);
    // Serialize message field [trajectory]
    bufferOffset = rtp_msgs.msg.PathStamped.serialize(obj.trajectory, buffer, bufferOffset);
    // Serialize message field [occ_grid]
    bufferOffset = nav_msgs.msg.OccupancyGrid.serialize(obj.occ_grid, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type ControlTask
    let len;
    let data = new ControlTask(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [current_pose]
    data.current_pose = nav_msgs.msg.Odometry.deserialize(buffer, bufferOffset);
    // Deserialize message field [estop]
    data.estop = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [stop]
    data.stop = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [trajectory]
    data.trajectory = rtp_msgs.msg.PathStamped.deserialize(buffer, bufferOffset);
    // Deserialize message field [occ_grid]
    data.occ_grid = nav_msgs.msg.OccupancyGrid.deserialize(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += nav_msgs.msg.Odometry.getMessageSize(object.current_pose);
    length += rtp_msgs.msg.PathStamped.getMessageSize(object.trajectory);
    length += nav_msgs.msg.OccupancyGrid.getMessageSize(object.occ_grid);
    return length + 2;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pnc_task_msgs/ControlTask';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ce735c9485fcfbbb8078084d79a91cf2';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    
    # Current robot position %
    nav_msgs/Odometry current_pose
    
    # Emergency stop %
    bool estop
    
    # Regular stop %
    bool stop
    
    # Trajectory %
    rtp_msgs/PathStamped trajectory
    
    # Map %
    nav_msgs/OccupancyGrid occ_grid
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: nav_msgs/Odometry
    # This represents an estimate of a position and velocity in free space.  
    # The pose in this message should be specified in the coordinate frame given by header.frame_id.
    # The twist in this message should be specified in the coordinate frame given by the child_frame_id
    Header header
    string child_frame_id
    geometry_msgs/PoseWithCovariance pose
    geometry_msgs/TwistWithCovariance twist
    
    ================================================================================
    MSG: geometry_msgs/PoseWithCovariance
    # This represents a pose in free space with uncertainty.
    
    Pose pose
    
    # Row-major representation of the 6x6 covariance matrix
    # The orientation parameters use a fixed-axis representation.
    # In order, the parameters are:
    # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
    float64[36] covariance
    
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: geometry_msgs/TwistWithCovariance
    # This expresses velocity in free space with uncertainty.
    
    Twist twist
    
    # Row-major representation of the 6x6 covariance matrix
    # The orientation parameters use a fixed-axis representation.
    # In order, the parameters are:
    # (x, y, z, rotation about X axis, rotation about Y axis, rotation about Z axis)
    float64[36] covariance
    
    ================================================================================
    MSG: geometry_msgs/Twist
    # This expresses velocity in free space broken into its linear and angular parts.
    Vector3  linear
    Vector3  angular
    
    ================================================================================
    MSG: geometry_msgs/Vector3
    # This represents a vector in free space. 
    # It is only meant to represent a direction. Therefore, it does not
    # make sense to apply a translation to it (e.g., when applying a 
    # generic rigid transformation to a Vector3, tf2 will only apply the
    # rotation). If you want your data to be translatable too, use the
    # geometry_msgs/Point message instead.
    
    float64 x
    float64 y
    float64 z
    ================================================================================
    MSG: rtp_msgs/PathStamped
    std_msgs/Header header
    
    # Массив, содержащий точки траектории с дополнительными свойствами
    rtp_msgs/PathPointWithMetadata[] path_with_metadata
    
    # Тип траектории
    uint8 path_type
    uint8 EUCLIDEAN = 0      # Константа, определяющая траекторию, спланированную в евклидовом пространстве
    uint8 DUBINS = 1         # Константа, определяющая траекторию, спланированную в пространстве кривых Дубинса
    uint8 REEDS_SHEPP = 2    # Константа, определяющая траекторию, спланированную в пространстве кривых Ридса-Шеппа
    
    # Индекс точки маршрута, к которой построена траектория
    uint16 route_index_to
    
    # Индекс точки маршрута, от которой построена траектория
    uint16 route_index_from
    ================================================================================
    MSG: rtp_msgs/PathPointWithMetadata
    # Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
    geometry_msgs/Pose pose
    
    # Дополнительные свойства точки маршрута/траектории
    rtp_msgs/PathPointMetadata metadata
    ================================================================================
    MSG: rtp_msgs/PathPointMetadata
    # Значение максимальной линейной скорости движения на сегменте маршрута
    float32 linear_velocity
    
    # Значение максимально допустимого отклонения от заданного маршрута/траектории
    float32 max_deviation
    
    # Признак «ключевая точка»
    bool key_point
    
    # Признак «точка возврата»
    bool return_point
    
    # Признак «точка, требующая от оператора подтверждения продолжения движения»
    bool confirmation_point
    
    # Заданное время стоянки в точке
    float32 delay
    ================================================================================
    MSG: nav_msgs/OccupancyGrid
    # This represents a 2-D grid map, in which each cell represents the probability of
    # occupancy.
    
    Header header 
    
    #MetaData for the map
    MapMetaData info
    
    # The map data, in row-major order, starting with (0,0).  Occupancy
    # probabilities are in the range [0,100].  Unknown is -1.
    int8[] data
    
    ================================================================================
    MSG: nav_msgs/MapMetaData
    # This hold basic information about the characterists of the OccupancyGrid
    
    # The time at which the map was loaded
    time map_load_time
    # The map resolution [m/cell]
    float32 resolution
    # Map width [cells]
    uint32 width
    # Map height [cells]
    uint32 height
    # The origin of the map [m, m, rad].  This is the real-world pose of the
    # cell (0,0) in the map.
    geometry_msgs/Pose origin
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new ControlTask(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.current_pose !== undefined) {
      resolved.current_pose = nav_msgs.msg.Odometry.Resolve(msg.current_pose)
    }
    else {
      resolved.current_pose = new nav_msgs.msg.Odometry()
    }

    if (msg.estop !== undefined) {
      resolved.estop = msg.estop;
    }
    else {
      resolved.estop = false
    }

    if (msg.stop !== undefined) {
      resolved.stop = msg.stop;
    }
    else {
      resolved.stop = false
    }

    if (msg.trajectory !== undefined) {
      resolved.trajectory = rtp_msgs.msg.PathStamped.Resolve(msg.trajectory)
    }
    else {
      resolved.trajectory = new rtp_msgs.msg.PathStamped()
    }

    if (msg.occ_grid !== undefined) {
      resolved.occ_grid = nav_msgs.msg.OccupancyGrid.Resolve(msg.occ_grid)
    }
    else {
      resolved.occ_grid = new nav_msgs.msg.OccupancyGrid()
    }

    return resolved;
    }
};

module.exports = ControlTask;
