// Auto-generated. Do not edit!

// (in-package pnc_task_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let nav_msgs = _finder('nav_msgs');
let rtp_msgs = _finder('rtp_msgs');
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class PlanningTask {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.init_point = null;
      this.goal_point = null;
      this.occ_grid = null;
      this.path_type = null;
      this.route_index_to = null;
      this.route_index_from = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('init_point')) {
        this.init_point = initObj.init_point
      }
      else {
        this.init_point = new rtp_msgs.msg.PathPointWithMetadata();
      }
      if (initObj.hasOwnProperty('goal_point')) {
        this.goal_point = initObj.goal_point
      }
      else {
        this.goal_point = new rtp_msgs.msg.PathPointWithMetadata();
      }
      if (initObj.hasOwnProperty('occ_grid')) {
        this.occ_grid = initObj.occ_grid
      }
      else {
        this.occ_grid = new nav_msgs.msg.OccupancyGrid();
      }
      if (initObj.hasOwnProperty('path_type')) {
        this.path_type = initObj.path_type
      }
      else {
        this.path_type = 0;
      }
      if (initObj.hasOwnProperty('route_index_to')) {
        this.route_index_to = initObj.route_index_to
      }
      else {
        this.route_index_to = 0;
      }
      if (initObj.hasOwnProperty('route_index_from')) {
        this.route_index_from = initObj.route_index_from
      }
      else {
        this.route_index_from = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type PlanningTask
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [init_point]
    bufferOffset = rtp_msgs.msg.PathPointWithMetadata.serialize(obj.init_point, buffer, bufferOffset);
    // Serialize message field [goal_point]
    bufferOffset = rtp_msgs.msg.PathPointWithMetadata.serialize(obj.goal_point, buffer, bufferOffset);
    // Serialize message field [occ_grid]
    bufferOffset = nav_msgs.msg.OccupancyGrid.serialize(obj.occ_grid, buffer, bufferOffset);
    // Serialize message field [path_type]
    bufferOffset = _serializer.uint8(obj.path_type, buffer, bufferOffset);
    // Serialize message field [route_index_to]
    bufferOffset = _serializer.uint16(obj.route_index_to, buffer, bufferOffset);
    // Serialize message field [route_index_from]
    bufferOffset = _serializer.uint16(obj.route_index_from, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type PlanningTask
    let len;
    let data = new PlanningTask(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [init_point]
    data.init_point = rtp_msgs.msg.PathPointWithMetadata.deserialize(buffer, bufferOffset);
    // Deserialize message field [goal_point]
    data.goal_point = rtp_msgs.msg.PathPointWithMetadata.deserialize(buffer, bufferOffset);
    // Deserialize message field [occ_grid]
    data.occ_grid = nav_msgs.msg.OccupancyGrid.deserialize(buffer, bufferOffset);
    // Deserialize message field [path_type]
    data.path_type = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [route_index_to]
    data.route_index_to = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [route_index_from]
    data.route_index_from = _deserializer.uint16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += nav_msgs.msg.OccupancyGrid.getMessageSize(object.occ_grid);
    return length + 147;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pnc_task_msgs/PlanningTask';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'fd40ef44d8eab5b0a4450060a66caae6';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    std_msgs/Header header
    
    # Init robot position %
    rtp_msgs/PathPointWithMetadata init_point
    
    # Goal robot position %
    rtp_msgs/PathPointWithMetadata goal_point
    
    # Map %
    nav_msgs/OccupancyGrid occ_grid
    
    # Options %
    uint8 path_type
    uint16 route_index_to       # Индекс точки маршрута, к которой построена траектория
    uint16 route_index_from     # Индекс точки маршрута, от которой построена траектория
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    ================================================================================
    MSG: rtp_msgs/PathPointWithMetadata
    # Пространственное положение точки маршрута/траектории (координаты и кватернион ориентации)
    geometry_msgs/Pose pose
    
    # Дополнительные свойства точки маршрута/траектории
    rtp_msgs/PathPointMetadata metadata
    ================================================================================
    MSG: geometry_msgs/Pose
    # A representation of pose in free space, composed of position and orientation. 
    Point position
    Quaternion orientation
    
    ================================================================================
    MSG: geometry_msgs/Point
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    
    ================================================================================
    MSG: geometry_msgs/Quaternion
    # This represents an orientation in free space in quaternion form.
    
    float64 x
    float64 y
    float64 z
    float64 w
    
    ================================================================================
    MSG: rtp_msgs/PathPointMetadata
    # Значение максимальной линейной скорости движения на сегменте маршрута
    float32 linear_velocity
    
    # Значение максимально допустимого отклонения от заданного маршрута/траектории
    float32 max_deviation
    
    # Признак «ключевая точка»
    bool key_point
    
    # Признак «точка возврата»
    bool return_point
    
    # Признак «точка, требующая от оператора подтверждения продолжения движения»
    bool confirmation_point
    
    # Заданное время стоянки в точке
    float32 delay
    ================================================================================
    MSG: nav_msgs/OccupancyGrid
    # This represents a 2-D grid map, in which each cell represents the probability of
    # occupancy.
    
    Header header 
    
    #MetaData for the map
    MapMetaData info
    
    # The map data, in row-major order, starting with (0,0).  Occupancy
    # probabilities are in the range [0,100].  Unknown is -1.
    int8[] data
    
    ================================================================================
    MSG: nav_msgs/MapMetaData
    # This hold basic information about the characterists of the OccupancyGrid
    
    # The time at which the map was loaded
    time map_load_time
    # The map resolution [m/cell]
    float32 resolution
    # Map width [cells]
    uint32 width
    # Map height [cells]
    uint32 height
    # The origin of the map [m, m, rad].  This is the real-world pose of the
    # cell (0,0) in the map.
    geometry_msgs/Pose origin
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new PlanningTask(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.init_point !== undefined) {
      resolved.init_point = rtp_msgs.msg.PathPointWithMetadata.Resolve(msg.init_point)
    }
    else {
      resolved.init_point = new rtp_msgs.msg.PathPointWithMetadata()
    }

    if (msg.goal_point !== undefined) {
      resolved.goal_point = rtp_msgs.msg.PathPointWithMetadata.Resolve(msg.goal_point)
    }
    else {
      resolved.goal_point = new rtp_msgs.msg.PathPointWithMetadata()
    }

    if (msg.occ_grid !== undefined) {
      resolved.occ_grid = nav_msgs.msg.OccupancyGrid.Resolve(msg.occ_grid)
    }
    else {
      resolved.occ_grid = new nav_msgs.msg.OccupancyGrid()
    }

    if (msg.path_type !== undefined) {
      resolved.path_type = msg.path_type;
    }
    else {
      resolved.path_type = 0
    }

    if (msg.route_index_to !== undefined) {
      resolved.route_index_to = msg.route_index_to;
    }
    else {
      resolved.route_index_to = 0
    }

    if (msg.route_index_from !== undefined) {
      resolved.route_index_from = msg.route_index_from;
    }
    else {
      resolved.route_index_from = 0
    }

    return resolved;
    }
};

module.exports = PlanningTask;
