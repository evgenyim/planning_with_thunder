#include <iostream>

#include "ros/ros.h"
#include "dynamic_reconfigure/server.h"
#include "geometry_msgs/PointStamped.h"
#include "visualization_msgs/Marker.h"

#include "test_package/ParamsConfig.h"

#include "pnc_task_msgs/PlanningTask.h"
#include "rtp_msgs/PathStamped.h"

using namespace std;
class TestClass
{
  private:
    
    ros::NodeHandle n; 
    ros::Publisher test_pub;
    ros::Subscriber test_sub;
    
    double r;

  public:
  
    TestClass();
    void run();
    void test_callback(const pnc_task_msgs::PlanningTask& task);
};
