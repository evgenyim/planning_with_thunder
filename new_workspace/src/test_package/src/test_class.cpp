#include "../include/test_class.h"
#include <set>
#include <algorithm>

const int N = 600;
const int INF = 1e9;

TestClass::TestClass() {    	
  test_pub = n.advertise<rtp_msgs::PathStamped>("/planner_sb_node/trajectory", 1);
  
  test_sub = n.subscribe("/planning_node/task", 10, &TestClass::test_callback, this);
}

void TestClass::run() {
  ros::spin();
}

void TestClass::test_callback(const pnc_task_msgs::PlanningTask& task) {
	ROS_ERROR("Init: [%f] [%f] [%f]", task.init_point.pose.position.x, task.init_point.pose.position.y, task.init_point.pose.position.z);
	
	ROS_ERROR("Goal: [%f] [%f] [%f]", task.goal_point.pose.position.x, task.goal_point.pose.position.y, task.goal_point.pose.position.z);
	int start_x = N / 2 - 1;
	int start_y = N / 2 - 1;
	float resol = task.occ_grid.info.resolution;
	int goal_x = start_x + std::round((task.goal_point.pose.position.x - task.init_point.pose.position.x) / resol);
	int goal_y = start_y + std::round((task.goal_point.pose.position.y - task.init_point.pose.position.y) / resol);
	ROS_ERROR("Grid: [%i, %i] [%i, %i]", start_x, start_y, goal_x, goal_y);
	
	std::set<std::pair<int, std::pair <int, int> > > s;
	s.insert({0, {start_x, start_y}});
	
	std::vector <std::vector <int> > d(N, std::vector<int> (N, INF));
	std::vector <std::vector <std::pair <int, int> > > parent(N, std::vector <std::pair <int, int> >(N, std::make_pair(-1, -1)));
	
	d[start_x][start_y] = 0;
	while (!s.empty()) {
		std::pair <int, std::pair <int, int> > p = *s.begin();
		s.erase(s.begin());
		if (p.first > d[p.second.first][p.second.second])
			continue;
		if (p.second.first == goal_x && p.second.second == goal_y)
			break;
		for (int dx = -1; dx <= 1; dx++)
			for (int dy = -1; dy <= 1; dy++)
				if (abs(dx) + abs(dy) == 1) {
					int nx = p.second.first + dx;
					int ny = p.second.second + dy;
					int cost = task.occ_grid.data[nx + ny * N];
					if (0 <= nx && nx < N && 0 <= ny && ny < N && cost < 51 && d[nx][ny] > p.first + cost + 1) {
						d[nx][ny] = p.first + cost + 1;
						parent[nx][ny] = p.second;
						s.insert({d[nx][ny], {nx, ny}});
					}
				}
	}
	
	if (d[goal_x][goal_y] == INF) {
		ROS_ERROR("No path");
		return;
	}
	std::vector <std::pair <int, int> > path;
	path.push_back({goal_x, goal_y});
	while (goal_x != start_x || goal_y != start_y) {
		int n_x = parent[goal_x][goal_y].first;
		int n_y = parent[goal_x][goal_y].second;
		goal_x = n_x;
		goal_y = n_y;
		path.push_back({goal_x, goal_y});
	}
	
	std::reverse(path.begin(), path.end());
	
	rtp_msgs::PathStamped msg;
	
	msg.path_type = task.path_type;
	
	msg.header = task.header;
	
	msg.route_index_to = task.route_index_to;
	msg.route_index_from = task.route_index_from;
	
	std::vector<rtp_msgs::PathPointWithMetadata> path_msg(path.size());
	
	for (int i = 0; i < (int)path.size(); i++) {
		path_msg[i].pose.position.x = task.init_point.pose.position.x + (path[i].first - start_x) * resol;
		path_msg[i].pose.position.y = task.init_point.pose.position.y + (path[i].second - start_y) * resol;
		path_msg[i].pose.position.z = 0;
		path_msg[i].pose.orientation.x = 0;
		path_msg[i].pose.orientation.y = 0;
		path_msg[i].pose.orientation.z = 0;
		path_msg[i].pose.orientation.w = 1;
		path_msg[i].metadata = task.goal_point.metadata;
	}
	
	msg.path_with_metadata = path_msg;
	
	test_pub.publish(msg);
}
