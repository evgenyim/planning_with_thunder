# Пример узла, принимающего и публикующего сообщения

## Инструкция по установке и запуску

1. Создать новую директорию, в которой будет содержаться рабочее пространство, в ней создать папку src.
```bash
cd ~
mkdir name_of_workspace
cd name_of_workspace
mkdir src
```

2. test_package положить в src

3. Собрать рабочее пространство
```bash
cd ~/name_of_workspace
catkin_make
```

4. Запустить test_node
```bash
rosrun test_package test_node
```

5. Добавить в rviz (в панели слева) отображение публикуемых сообщений через Add -> By topic -> /test_node -> Marker

6. После этого при нажатии кнопки интерфейса Publish point в месте нажатия будет появляться красный кружок